package com.ladorian.marinedacity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

public class ServiciosFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.servicios_container, null);
		  final ExpandableListView serviciosList= (ExpandableListView) v.findViewById(R.id.serviciosList);
	        serviciosList.setAdapter(new ServiciosListAdapter());
		return v;
	}
	
	public class ServiciosListAdapter extends BaseExpandableListAdapter {
		private final List <Map<String,Object>> group;
		
		
		public ServiciosListAdapter() {
			super();
			group= new ArrayList<Map<String,Object>>();
			final Map <String,Object> map1 = new HashMap<String,Object>();
			map1.put("title", "CON NIÑOS");
			map1.put("subtitle", "Servicios para las familias");
			map1.put("icon",R.drawable.serv_familias);
			
			
			final List <Map<String,Object>> child1 = new ArrayList<Map<String,Object>> ();
			final Map <String,Object> cmap1 = new HashMap<String,Object>();
			cmap1.put("title","Pulsera de seguridad infantil");
			cmap1.put("icon",R.drawable.serv_pulsera);
			child1.add(cmap1);
			final Map <String,Object> cmap2 = new HashMap<String,Object>();
			cmap2.put("title","Aseos infantiles");
			cmap2.put("icon",R.drawable.serv_aseos_infantiles);
			child1.add(cmap2);
			final Map <String,Object> cmap3 = new HashMap<String,Object>();
			cmap3.put("title","Cambiadores bebés");
			cmap3.put("icon",R.drawable.serv_cambiadores);
			child1.add(cmap3);
			final Map <String,Object> cmap4 = new HashMap<String,Object>();
			cmap4.put("title","Préstamo sillas para bebés");
			cmap4.put("icon",R.drawable.serv_prestamo_sillas);
			child1.add(cmap4);
			final Map <String,Object> cmap5 = new HashMap<String,Object>();
			cmap5.put("title","Salas de lactancia");
			cmap5.put("icon",R.drawable.serv_salas_lactancia);
			child1.add(cmap5);
			
			map1.put("child", child1);
			group.add(map1);
			
			
			final Map <String,Object> map2 = new HashMap<String,Object>();
			map2.put("title", "MOVILIDAD REDUCIDA");
			map2.put("subtitle", "Servicios especiales");
			map2.put("icon",R.drawable.serv_movilidad);
			final List <Map<String,Object>> child2 = new ArrayList<Map<String,Object>> ();
			final Map <String,Object> cmap6 = new HashMap<String,Object>();
			cmap6.put("title","Ascensores Plaza Central y Plaza Elíptica");
			cmap6.put("icon",R.drawable.serv_ascensores);
			child2.add(cmap6);
			final Map <String,Object> cmap7 = new HashMap<String,Object>();
			cmap7.put("title","Escaleras mecánicas");
			cmap7.put("icon",R.drawable.serv_escaleras_mecanicas);
			child2.add(cmap7);
			final Map <String,Object> cmap8 = new HashMap<String,Object>();
			cmap8.put("title","Plazas de parking movilidad reducida");
			cmap8.put("icon",R.drawable.serv_movilidad);
			child2.add(cmap8);
			final Map <String,Object> cmap9 = new HashMap<String,Object>();
			cmap9.put("title","Scooters eléctricas para personas con movilidad reducida");
			cmap9.put("icon",R.drawable.serv_scooters);
			child2.add(cmap9);
			final Map <String,Object> cmap10 = new HashMap<String,Object>();
			cmap10.put("title","Préstamo silla de ruedas");
			cmap10.put("icon",R.drawable.serv_prestamo_sillas_ruedas);
			child2.add(cmap10);
			
			map2.put("child", child2);
			group.add(map2);
			
			final Map <String,Object> map3 = new HashMap<String,Object>();
			map3.put("title", "SERVICIOS CONFORT");
			map3.put("subtitle", "Para tu comodidad");
			map3.put("icon",R.drawable.serv_hotel);
			final List <Map<String,Object>> child3 = new ArrayList<Map<String,Object>> ();
			final Map <String,Object> cmap11 = new HashMap<String,Object>();
			cmap11.put("title","Hotel");
			cmap11.put("icon",R.drawable.serv_hotel);
			child3.add(cmap11);
//			final Map <String,Object> cmap12 = new HashMap<String,Object>();
//			cmap12.put("title","Recuerda tu plaza de parking");
//			cmap12.put("icon",R.drawable.serv_recuerda_plaza);
//			child3.add(cmap12);
			final Map <String,Object> cmap13 = new HashMap<String,Object>();
			cmap13.put("title","Indicadores luminosos de plazas de parking");
			cmap13.put("icon",R.drawable.serv_indicadores_luminosos);
			child3.add(cmap13);
			final Map <String,Object> cmap14 = new HashMap<String,Object>();
			cmap14.put("title","Indicadores de plazas disponibles");
			cmap14.put("icon",R.drawable.serv_indicadores_plazas);
			child3.add(cmap14);
			final Map <String,Object> cmap15 = new HashMap<String,Object>();
			cmap15.put("title","Aparca bicis en plaza exterior");
			cmap15.put("icon",R.drawable.serv_aparca_bicis);
			child3.add(cmap15);
			final Map <String,Object> cmap16 = new HashMap<String,Object>();
			cmap16.put("title","Plazas para motos");
			cmap16.put("icon",R.drawable.serv_plazas_motos);
			child3.add(cmap16);
			final Map <String,Object> cmap17 = new HashMap<String,Object>();
			cmap17.put("title","Áreas de descanso");
			cmap17.put("icon",R.drawable.serv_areas_descanso);
			child3.add(cmap17);
			final Map <String,Object> cmap18 = new HashMap<String,Object>();
			cmap18.put("title","Taquillas");
			cmap18.put("icon",R.drawable.serv_taquillas);
			child3.add(cmap18);
			
			map3.put("child", child3);			
			group.add(map3);
			
			final Map <String,Object> map4 = new HashMap<String,Object>();
			map4.put("title", "SERVICIOS MARINEDA");
			map4.put("subtitle", "Recursos prácticos");
			map4.put("icon",R.drawable.serv_puntos_atencion);
			final List <Map<String,Object>> child4 = new ArrayList<Map<String,Object>> ();
			final Map <String,Object> cmap19 = new HashMap<String,Object>();
			cmap19.put("title","Aseos en todas las plantas");
			cmap19.put("icon",R.drawable.serv_aseos);
			child4.add(cmap19);
			final Map <String,Object> cmap20 = new HashMap<String,Object>();
			cmap20.put("title","Cardio Protección");
			cmap20.put("icon",R.drawable.serv_cardio);
			child4.add(cmap20);
			final Map <String,Object> cmap21 = new HashMap<String,Object>();
			cmap21.put("title","2 Puntos de Atención");
			cmap21.put("icon",R.drawable.serv_puntos_atencion);
			child4.add(cmap21);
			final Map <String,Object> cmap22 = new HashMap<String,Object>();
			cmap22.put("title","Objetos Perdidos");
			cmap22.put("icon",R.drawable.serv_objetos_perdidos);
			child4.add(cmap22);
			final Map <String,Object> cmap23 = new HashMap<String,Object>();
			cmap23.put("title","Marineda City Card");
			cmap23.put("icon",R.drawable.serv_mccard);
			child4.add(cmap23);
			final Map <String,Object> cmap24 = new HashMap<String,Object>();
			cmap24.put("title","Empaquetado de regalos");
			cmap24.put("icon",R.drawable.serv_empaquetado);
			child4.add(cmap24);
			final Map <String,Object> cmap25 = new HashMap<String,Object>();
			cmap25.put("title","Pantallas informativas táctiles");
			cmap25.put("icon",R.drawable.serv_pantallas_informativas);
			child4.add(cmap25);
			final Map <String,Object> cmap26 = new HashMap<String,Object>();
			cmap26.put("title","Cajetos automáticos en las 3 plantas");
			cmap26.put("icon",R.drawable.serv_cajeros);
			child4.add(cmap26);
			final Map <String,Object> cmap27 = new HashMap<String,Object>();
			cmap27.put("title","Teléfonos públicos");
			cmap27.put("icon",R.drawable.serv_telefonos);
			child4.add(cmap27);
			final Map <String,Object> cmap28 = new HashMap<String,Object>();
			cmap28.put("title","Wifi");
			cmap28.put("icon",R.drawable.serv_wifi);
			child4.add(cmap28);
			final Map <String,Object> cmap29 = new HashMap<String,Object>();
			cmap29.put("title","Buzón de Correos");
			cmap29.put("icon",R.drawable.serv_buzon);
			child4.add(cmap29);
			final Map <String,Object> cmap30 = new HashMap<String,Object>();
			cmap30.put("title","Farmacia");
			cmap30.put("icon",R.drawable.serv_farmacias);
			child4.add(cmap30);
			final Map <String,Object> cmap31 = new HashMap<String,Object>();
			cmap31.put("title","Agencia de Viajes");
			cmap31.put("icon",R.drawable.serv_agencia_viajes);
			child4.add(cmap31);
			final Map <String,Object> cmap32 = new HashMap<String,Object>();
			cmap32.put("title","Espacio 2.0");
			cmap32.put("icon",R.drawable.serv_espacio);
			child4.add(cmap32);
			final Map <String,Object> cmap33 = new HashMap<String,Object>();
			cmap33.put("title","Marineda Bookcrossing");
			cmap33.put("icon",R.drawable.serv_bookcrossing);
			child4.add(cmap33);
			final Map <String,Object> cmap34 = new HashMap<String,Object>();
			cmap34.put("title","5 entradas y 5 salidas");
			cmap34.put("icon",R.drawable.serv_entradas);
			child4.add(cmap34);
			final Map <String,Object> cmap35 = new HashMap<String,Object>();
			cmap35.put("title","Lavado de coches");
			cmap35.put("icon",R.drawable.serv_lavado_coches);
			child4.add(cmap35);
			final Map <String,Object> cmap36 = new HashMap<String,Object>();
			cmap36.put("title","Pinzas arranca coches");
			cmap36.put("icon",R.drawable.serv_pinzas_arrancar);
			child4.add(cmap36);
			
			map4.put("child", child4);
			group.add(map4);
		}

		@Override
		public int getGroupCount() {
			// TODO Auto-generated method stub
			return group.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			// TODO Auto-generated method stub
			Map <String,Object> groupMap = group.get(groupPosition);
			List<Map<String,Object>> childList = (List<Map<String, Object>>) groupMap.get("child");
			if(childList != null) return childList.size();
			return 0;
		}

		@Override
		public Object getGroup(int groupPosition) {
			// TODO Auto-generated method stub
			return group.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			Map <String,Object> groupMap = group.get(groupPosition);
			List<Map<String,Object>> childList = (List<Map<String, Object>>)groupMap.get("child");
			if(childList !=null) return childList.get(childPosition);
			return null;
		}

		@Override
		public long getGroupId(int i) {
			// TODO Auto-generated method stub
			return i;
		}

		@Override
		public long getChildId(int i, int i1) {
			// TODO Auto-generated method stub
			return i1;
		}

		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			if (convertView == null) {
     		   convertView = getActivity().getLayoutInflater().inflate(R.layout.servicios_group_cell, null);
     		  }
			Map <String,Object> map = group.get(groupPosition);
			TextView titleView = (TextView)convertView.findViewById(R.id.titleLabel);
			titleView.setText((String)map.get("title"));
			TextView subtitleView = (TextView)convertView.findViewById(R.id.subtitleLabel);
			subtitleView.setText((String)map.get("subtitle"));
			ImageView icon = (ImageView)convertView.findViewById(R.id.iconImage);
			icon.setImageResource((Integer)map.get("icon"));
			if(isExpanded) {
				convertView.setBackgroundColor(R.color.dark_gray);
				titleView.setTextColor(Color.WHITE);
				subtitleView.setTextColor(Color.WHITE);
			} else {
				convertView.setBackgroundColor(android.R.color.white);
				titleView.setTextColor(Color.BLACK);
				subtitleView.setTextColor(Color.BLACK);
			}
			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(R.layout.servicios_child_cell, null);
			}
			Map <String,Object> map = (Map<String, Object>) this.getChild(groupPosition, childPosition);
			TextView titleView = (TextView)convertView.findViewById(R.id.childTitleLabel);
			titleView.setText((String)map.get("title"));
			ImageView icon = (ImageView)convertView.findViewById(R.id.childIconImage);
			icon.setImageResource((Integer)map.get("icon"));
			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return false;
		}
		
	}

}
