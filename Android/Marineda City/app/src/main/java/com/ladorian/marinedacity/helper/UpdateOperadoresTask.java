package com.ladorian.marinedacity.helper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.ladorian.marinedacity.ApiClient;
import com.ladorian.marinedacity.model.Operador;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

public class UpdateOperadoresTask extends AsyncTask<Void, Void, Void> {
	
	private Context mContext;
	
	public UpdateOperadoresTask (Context context) {
		mContext = context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		final SharedPreferences prefs = mContext.getSharedPreferences("firstRun", Context.MODE_PRIVATE);
		String updated = prefs.getString("updated", null);
		final DatabaseHelper db = new DatabaseHelper(mContext);
		if(updated != null) {
			ApiClient.getMarinedaCityApiClient().updateOperadores(updated, new Callback<List<List<Operador>>> () {

				@Override
				public void failure(RetrofitError arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void success(List<List<Operador>> arg0, Response arg1) {
					if(arg0.get(0) != null) {
						Log.d("Operador a actualizar:",((Operador)arg0.get(0).get(0)).getNombre());
						for(Operador operador : arg0.get(0)) {
							if(db.isSaved(operador.getId()) > 0) {
								db.updateOperador(operador);
							} else {
								try {
									db.createOperador(operador);
								} catch (ClientProtocolException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
					}
					if(arg0.size()>1) {
						if(arg0.get(1) != null && arg0.get(1).size()>0) {
							for (Operador operador : arg0.get(1)) {
								db.deleteOperador(operador.getId());
							}
						}
					}
					db.closeDB();
					SharedPreferences.Editor editor = prefs.edit();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String currentDate = sdf.format(new Date());
					editor.putString("updated", currentDate);
					editor.commit();
				}
				
			});
		}
		return null;
	}

}
