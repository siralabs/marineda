package com.ladorian.marinedacity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;


import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class ComoLLegarFragment extends Fragment implements OnChildClickListener,
        LocationListener, OnGroupClickListener {

    private Context mAppContext;
    private LocationManager mLocationManager;
    private String getURL;
    private String mParadas;
    private ArrayList<ParadaBus> paradasList;
    private Location mLocation;
    private final LLegarListAdapter mAdapter = new LLegarListAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mAppContext = getActivity().getApplicationContext();
        mLocationManager = (LocationManager) mAppContext.getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        String provider = mLocationManager.getBestProvider(criteria, true);
        mLocationManager.requestLocationUpdates(provider, 0, 0, this);
        mLocation = mLocationManager.getLastKnownLocation(provider);

        if (mLocation != null) {
            this.onLocationChanged(mLocation);
            String clientCoordinate = mLocation.getLatitude() + "_" + mLocation.getLongitude() + "_5000_5";
            getURL = "http://itranvias.com/queryitr.php?&func=3&dato=" + clientCoordinate;
        //    getURL = "http://itranvias.com/queryitr.php?&func=3&dato=43.362676_-8.408078_5000_5";
        } else {
            // leads to the settings because there is no last known location
            //Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            //startActivity(intent);
            mLocationManager.requestLocationUpdates(provider, 0, 0, this);
            mLocation = mLocationManager.getLastKnownLocation(provider);
            if (mLocation != null) {
                this.onLocationChanged(mLocation);
                String clientCoordinate = mLocation.getLatitude() + "_" + mLocation.getLongitude() + "_5000_5";
                getURL = "http://itranvias.com/queryitr.php?&func=3&dato=" + clientCoordinate;
          //      getURL = "http://itranvias.com/queryitr.php?&func=3&dato=43.362676_-8.408078_5000_5";
            }
        }
        // location updates: at least 50 meter and 200millsecs change
        //mLocationManager.requestLocationUpdates(provider, 200, 50, this);

        View v = inflater.inflate(R.layout.llegar_container, null);
        final ExpandableListView llegarList = (ExpandableListView) v.findViewById(R.id.llegarList);
        llegarList.setAdapter(mAdapter);
        llegarList.setOnChildClickListener(this);
        llegarList.setOnGroupClickListener(this);

        return v;
    }

    private class TranviasTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String type = params[0];
            String address = params[1];
            StringBuilder builder = new StringBuilder();
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(address);
            try {
                HttpResponse response = client.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                } else {
                    Log.e(MainActivity.class.toString(), "Failedet JSON object");
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mParadas = result;
            addParadas();
        }

        ;
    }

    @SuppressWarnings("unchecked")
    public void addParadas() {
        paradasList = new ArrayList<ParadaBus>();
//        if (mParadas == null) {
//            if(getURL==null){
//                getURL = "http://itranvias.com/queryitr.php?&func=3&dato=43.362676_-8.408078_5000_5";
//            }
//            new TranviasTask().execute("paradas", getURL);
//
//        }
        if (mParadas != null) {
            try {
                JSONObject jsonObject = new JSONObject(mParadas);
                JSONArray paradasArray = jsonObject.getJSONArray("paradas");

                for (int i = 0; i < paradasArray.length(); i++) {
                    ParadaBus parada = new ParadaBus();
                    parada.setmId(paradasArray.getJSONObject(i).getString("id"));
                    parada.setmNombre(paradasArray.getJSONObject(i).getString("parada"));
                    parada.setmDistancia(paradasArray.getJSONObject(i).getString("distancia"));
                    parada.setmPosx(paradasArray.getJSONObject(i).getString("posx"));
                    parada.setmPosy(paradasArray.getJSONObject(i).getString("posy"));
                    paradasList.add(parada);
                }
                Collections.sort(paradasList, new Comparator() {

                    @Override
                    public int compare(Object lhs, Object rhs) {
                        final Integer i1 = Integer.parseInt(((ParadaBus) lhs).getmDistancia());
                        final Integer i2 = Integer.parseInt(((ParadaBus) rhs).getmDistancia());
                        return new Integer(i1).compareTo(new Integer(i2));
                    }


                });
                mAdapter.addParadas(paradasList);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                System.out.println("Success");
            }
        }
    }

    public class ParadaBus {
        private String mNombre;
        private String mId;
        private String mDistancia;
        private String mPosx;
        private String mPosy;

        public String getmPosx() {
            return mPosx;
        }

        public void setmPosx(String mPosx) {
            this.mPosx = mPosx;
        }

        public String getmPosy() {
            return mPosy;
        }

        public void setmPosy(String mPosy) {
            this.mPosy = mPosy;
        }

        public String getmDistancia() {
            return mDistancia;
        }

        public void setmDistancia(String mDistancia) {
            this.mDistancia = mDistancia;
        }

        public String getmNombre() {
            return mNombre;
        }

        public void setmNombre(String mNombre) {
            this.mNombre = mNombre;
        }

        public String getmId() {
            return mId;
        }

        public void setmId(String mId) {
            this.mId = mId;
        }


    }

    @Override
    public void onResume() {

        super.onResume();
        // new TranviasTask().execute("paradas",getURL);
    }


    @Override
    public boolean onChildClick(ExpandableListView parent, View v,
                                int groupPosition, int childPosition, long id) {
        switch (groupPosition) {
            case 0:
                llamarAGMaps(parent, groupPosition, childPosition, id);
                break;
            case 1:
                String phoneString = ((String) ((Map<String, Object>) ((ExpandableListAdapter) parent.getExpandableListAdapter()).getChild(groupPosition, childPosition)).get("subtitle")).replaceAll("\\s+", "");
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneString));
                startActivity(callIntent);
                break;
            default:
                LLegarListAdapter adapter = (LLegarListAdapter) parent.getExpandableListAdapter();
                ParadaBus parada = (ParadaBus) adapter.getChild(groupPosition, childPosition);
                ((MainActivity) getActivity()).openParada(parada.getmId(), parada.getmNombre(), parada.getmPosx(), parada.getmPosy(), mLocation);
                parent.collapseGroup(groupPosition);
                break;

        }
        return false;
    }

    protected void llamarAGMaps(final ExpandableListView parent, final int groupPosition, int childPosition, long id) {
        if (childPosition == 3) {
            String strUri = "http://maps.google.com/maps?q=loc:43.34458,-8.42760 (" + "Marineda City" + ")";
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));

            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                try {
                    Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                    startActivity(unrestrictedIntent);
                } catch (ActivityNotFoundException innerEx) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Abriendo mapas")
                            .setMessage("Instale un aplicación de mapas.")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    parent.collapseGroup(groupPosition);
                                }
                            })
                            .show();
                    ;
                }
            }
        }
    }


    @Override
    public boolean onGroupClick(final ExpandableListView parent, View v,
                                final int groupPosition, long id) {
        if (groupPosition == 2 && paradasList == null) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Cargando paradas")
                    .setMessage("Por favor, espere unos segundos.")
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            parent.collapseGroup(groupPosition);
                        }
                    })
                    .show();
            addParadas();
            onGroupClick(parent, v, groupPosition, id);
        } else if (groupPosition == 2 && paradasList.size() == 0) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Línea 11")
                    .setMessage("No hay paradas de esta línea cerca de usted.")
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            parent.collapseGroup(groupPosition);
                        }
                    })
                    .show();
        }

        return false;
    }

    public class LLegarListAdapter extends BaseExpandableListAdapter {
        private final List<Map<String, Object>> group;
        private ArrayList<ParadaBus> mParadas;

        public void addParadas(ArrayList<ParadaBus> paradas) {
            mParadas = paradas;
            this.notifyDataSetChanged();
        }

        public LLegarListAdapter() {
            super();
            group = new ArrayList<Map<String, Object>>();
            final Map<String, Object> map1 = new HashMap<String, Object>();
            map1.put("title", "EN COCHE");
            map1.put("subtitle", "Desde la AP-9, A-6 o A Coruña");
            map1.put("icon", R.drawable.trans_coche);

            final List<Map<String, Object>> child1 = new ArrayList<Map<String, Object>>();
            final Map<String, Object> cmap1 = new HashMap<String, Object>();
            cmap1.put("title", "Desde la AP-9:");
            cmap1.put("subtitle", "Avd.Alfonso Molina. \nEn la rotonda de Eduardo Diz López, salida dirección Carretera de Baños Arteixo.\n\nAP-9 Salida 16, dirección Autovía de Arteixo. Salida km 592.\nDirección AG-55 A Coruña.");
            cmap1.put("icon", R.drawable.trans_coche);
            child1.add(cmap1);
            final Map<String, Object> cmap2 = new HashMap<String, Object>();
            cmap2.put("title", "Desde la autovía A-6:");
            cmap2.put("subtitle", "A6 Dirección Arteixo. Salida km 592.\nDirección AG-55 dirección A Coruña.");
            cmap2.put("icon", R.drawable.trans_coche);
            child1.add(cmap2);
            final Map<String, Object> cmap3 = new HashMap<String, Object>();
            cmap3.put("title", "Desde A Coruña:");
            cmap3.put("subtitle", "Desde la Avda. de Alfonso Molina, salida Polígono de A Grela-Bens e incorporación a la Avda. San Cristóbal.\n\nEn la rotonda de Eduardo Diz López, salida dirección Caretera de Baños-Arteixo.");
            cmap3.put("icon", R.drawable.trans_coche);
            child1.add(cmap3);
            final Map<String, Object> cmap14 = new HashMap<String, Object>();
            cmap14.put("title", "IR A MAPA ->");
            cmap14.put("subtitle", "");
            cmap14.put("icon", R.drawable.trans_coche);
            child1.add(cmap14);
            map1.put("child", child1);

            group.add(map1);
            final List<Map<String, Object>> child2 = new ArrayList<Map<String, Object>>();
            final Map<String, Object> map2 = new HashMap<String, Object>();
            map2.put("title", "EN TAXI");
            map2.put("subtitle", "Compañías");
            map2.put("icon", R.drawable.trans_taxi);
            final Map<String, Object> cmap4 = new HashMap<String, Object>();
            cmap4.put("title", "RADIO TAXI");
            cmap4.put("subtitle", "981 243 333");
            cmap4.put("icon", R.drawable.serv_telefonos);
            child2.add(cmap4);
            final Map<String, Object> cmap5 = new HashMap<String, Object>();
            cmap5.put("title", "TELE TAXI");
            cmap5.put("subtitle", "981 287 777");
            cmap5.put("icon", R.drawable.serv_telefonos);
            child2.add(cmap5);
            map2.put("child", child2);
            group.add(map2);

            final Map<String, Object> map3 = new HashMap<String, Object>();
            map3.put("title", "AUTOBÚS");
            map3.put("subtitle", "Línea 11 - Ver paradas");
            map3.put("icon", R.drawable.trans_bus);

            group.add(map3);

            final Map<String, Object> map4 = new HashMap<String, Object>();
            map4.put("title", "EN TREN");
            map4.put("subtitle", "Desde la estación de San Cristóbal");
            map4.put("icon", R.drawable.trans_train);
            final List<Map<String, Object>> child4 = new ArrayList<Map<String, Object>>();
            final Map<String, Object> cmap41 = new HashMap<String, Object>();
            cmap41.put("title", "Desde la estación de San Cristóbal:");
            cmap41.put("subtitle", "Tomar el autobús línea 11 en la parada de Los Mallos o en taxi hasta Marineda City (10 minutos)");
            cmap41.put("icon", R.drawable.trans_train);
            child4.add(cmap41);
            map4.put("child", child4);
            group.add(map4);

            final Map<String, Object> map5 = new HashMap<String, Object>();
            map5.put("title", "EN AVIÓN");
            map5.put("subtitle", "Aeropuertos");
            map5.put("icon", R.drawable.trans_plane);
            final List<Map<String, Object>> child3 = new ArrayList<Map<String, Object>>();
            final Map<String, Object> cmap31 = new HashMap<String, Object>();
            cmap31.put("title", "Desde el aeropuerto de A Coruña (Alvedro):");
            cmap31.put("subtitle", "9,5 km (Por la Tercera Ronda 7 minutos en coche o taxi)");
            cmap31.put("icon", R.drawable.trans_plane);
            child3.add(cmap31);
            final Map<String, Object> cmap32 = new HashMap<String, Object>();
            cmap32.put("title", "Desde el aeropuerto de Santiago (Lavacolla):");
            cmap32.put("subtitle", "66,8 km");
            cmap32.put("icon", R.drawable.trans_plane);
            child3.add(cmap32);
            final Map<String, Object> cmap33 = new HashMap<String, Object>();
            cmap33.put("title", "Desde el aeropuerto de Vigo (Peinador):");
            cmap33.put("subtitle", "160 km");
            cmap33.put("icon", R.drawable.trans_plane);
            child3.add(cmap33);
            map5.put("child", child3);
            group.add(map5);
        }

        @Override
        public int getGroupCount() {
            // TODO Auto-generated method stub
            return group.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            if (groupPosition == 2 && mParadas != null) {
                return mParadas.size();
            } else {
                Map<String, Object> groupMap = group.get(groupPosition);
                List<Map<String, Object>> childList = (List<Map<String, Object>>) groupMap.get("child");
                if (childList != null) {
                    return childList.size();
                }
            }
            return 0;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return group.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            if (groupPosition == 2 && mParadas != null) {
                ParadaBus parada = mParadas.get(childPosition);
                return parada;
            } else {
                Map<String, Object> groupMap = group.get(groupPosition);
                List<Map<String, Object>> childList = (List<Map<String, Object>>) groupMap.get("child");
                if (childList != null) return childList.get(childPosition);
            }

            return null;
        }

        @Override
        public long getGroupId(int i) {
            // TODO Auto-generated method stub
            return i;
        }

        @Override
        public long getChildId(int i, int i1) {
            // TODO Auto-generated method stub
            return i1;
        }

        @Override
        public boolean hasStableIds() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.servicios_group_cell, null);
            }
            Map<String, Object> map = group.get(groupPosition);
            TextView titleView = (TextView) convertView.findViewById(R.id.titleLabel);
            titleView.setText((String) map.get("title"));
            TextView subtitleView = (TextView) convertView.findViewById(R.id.subtitleLabel);
            subtitleView.setText((String) map.get("subtitle"));
            ImageView icon = (ImageView) convertView.findViewById(R.id.iconImage);
            icon.setImageResource((Integer) map.get("icon"));
            if (isExpanded) {
                convertView.setBackgroundColor(R.color.dark_gray);
                titleView.setTextColor(Color.WHITE);
                subtitleView.setTextColor(Color.WHITE);
            } else {
                convertView.setBackgroundColor(android.R.color.white);
                titleView.setTextColor(Color.BLACK);
                subtitleView.setTextColor(Color.BLACK);
            }
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.llegar_child_cell, null);
            }
            TextView titleView = (TextView) convertView.findViewById(R.id.childTitleLabel);
            TextView subtitleView = (TextView) convertView.findViewById(R.id.childSubtitleLabel);
            ImageView icon = (ImageView) convertView.findViewById(R.id.childIconImage);
            if (groupPosition == 2 && mParadas != null) {
                ParadaBus parada = (ParadaBus) this.getChild(groupPosition, childPosition);
                titleView.setText(parada.getmNombre());
                subtitleView.setText("Distancia " + parada.getmDistancia() + "m");
                icon.setImageResource(R.drawable.trans_bus);
            } else {
                Map<String, Object> map = (Map<String, Object>) this.getChild(groupPosition, childPosition);
                titleView.setText((String) map.get("title"));
                subtitleView.setText((String) map.get("subtitle"));
                icon.setImageResource((Integer) map.get("icon"));
            }

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            boolean isSelectable = false;
            switch (groupPosition) {
                case 0:
                    switch (childPosition) {
                        case 3:
                            isSelectable = true;
                            break;
                    }
                    break;
                case 3:
                case 4:
                    isSelectable = false;
                    break;
                default:
                    isSelectable = true;
                    break;
            }
            return isSelectable;
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        if (paradasList != null) {
            paradasList = null;
        }
        mLocationManager.removeUpdates(this);
        if (location != null) {
            mLocation = location;
            String clientCoordinate = location.getLatitude() + "_" + location.getLongitude() + "_5000_5";
            getURL = "http://itranvias.com/queryitr.php?&func=3&dato=" + clientCoordinate;
           // getURL = "http://itranvias.com/queryitr.php?&func=3&dato=43.362676_-8.408078_5000_5";
            new TranviasTask().execute("paradas", getURL);
        } else {
            //getURL = "http://itranvias.com/queryitr.php?&func=3&dato=43.362676_-8.408078_5000_5";
        }

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

}
