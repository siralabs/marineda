package com.ladorian.marinedacity;

import java.util.HashMap;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class RightMenuFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.right_menu_sample, null);
        final ListView elv = (ListView) v.findViewById(R.id.rightList);
        String[] values = new String[] { "INFORMACIÓN","SERVICIOS MARINEDA","¿CÓMO LLEGAR?","MI MARINEDA CITY CARD","MIS FAVORITOS",/*"REDES SOCIALES","REVISTA M-ALL",*/"NEWSLETTER",/*"LECTOR CÓDIGOS QR","TU OPINIÓN",*/"CONFIGURACIÓN","AVISO LEGAL" };
        
        
        
            final RightMenuListAdapter adapter = new RightMenuListAdapter(this.getActivity(),R.layout.child_row, values);
                elv.setAdapter(adapter);
        //elv.setAdapter(new RightMenuListAdapter());
                
        elv.setOnItemClickListener(new OnItemClickListener () {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				((MainActivity)getActivity()).changeFragment(0, position, "none",1);				
			}
        	
        });
                
        return v;
	}
	public class RightMenuListAdapter extends ArrayAdapter<String> {
		 //HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
		 	private final Context context;
		 	private final String[] values;
		    public RightMenuListAdapter(Context context, int textViewResourceId, String[] objects) {
		      super(context, textViewResourceId, objects);
		      this.context = context;
		      this.values = objects;
		    }
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if(convertView == null) {
				    convertView = getActivity().getLayoutInflater().inflate(R.layout.right_child_row, null);
				}
				    TextView text = (TextView) convertView.findViewById(R.id.child_row_name);
				    text.setText(values[position]);
				    ImageView iconView = (ImageView)convertView.findViewById(R.id.child_right_menu_icon);
				    switch(position) {
					    case 0:
					    	iconView.setImageResource(R.drawable.informacion);
					    	break;
					    case 1:
					    	iconView.setImageResource(R.drawable.serviciosmarineda);
					    	break;
					    case 2:
					    	iconView.setImageResource(R.drawable.comollegar);
					    	break;
					    case 3:
					    	iconView.setImageResource(R.drawable.mimarinedacitycard);
					    	break;
					    case 4:
					    	iconView.setImageResource(R.drawable.misfavoritos);
					    	break;
					    /*case 5:
					    	iconView.setImageResource(R.drawable.redessociales);
					    	break;
					    case 5:
					    	iconView.setImageResource(R.drawable.revistamall);
					    	break;*/
					    case 5:
					    	iconView.setImageResource(R.drawable.newsletter);
					    	break;
					    /*case 8:
					    	iconView.setImageResource(R.drawable.lectorqr);
					    	break;
					    case 9:
					    	iconView.setImageResource(R.drawable.tuopinion);
					    	break;*/
					    case 6:
					    	iconView.setImageResource(R.drawable.configuracion);
					    	break;
					    case 7:
					    	iconView.setImageResource(R.drawable.avisolegal);
					    	break;
				    }
				    return convertView;
			}
		    
	}
}
