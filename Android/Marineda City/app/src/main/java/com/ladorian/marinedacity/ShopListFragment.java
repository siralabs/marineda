package com.ladorian.marinedacity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ladorian.marinedacity.helper.DatabaseHelper;
import com.ladorian.marinedacity.model.Operador;


public class ShopListFragment extends ListFragment implements SearchView.OnQueryTextListener {
	public String mTitulo;
	private SearchView mSearchView;
	private ListView mListView;
	private ShopListAdapter mAdapter;
	DatabaseHelper db;
	private List<Operador> mOperadores;
	
	private boolean isMap;

	public String getmTitulo() {
		return mTitulo;
	}



	public void setmTitulo(String mTitulo) {
		this.mTitulo = mTitulo;
	}
	
	public ShopListFragment (String titulo)	{
		mTitulo = titulo;
		isMap = false;
	}
	
	public ShopListFragment (String titulo, boolean map) {
		mTitulo = titulo;
		isMap = map;
	}
	
	



	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mAdapter = null;
	}



	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		db = new DatabaseHelper(getActivity().getApplicationContext());
		if (mTitulo.equals("VER TIENDAS (A-Z)") || mTitulo.equals("ESTOY AQUÍ")) {
			mOperadores = db.getAllOperadores();
		} else {
			mOperadores = db.getCategoryOperadores(mTitulo);
		}
		db.closeDB();
		View v = inflater.inflate(R.layout.shop_list, null);
		TextView title = (TextView)v.findViewById(R.id.contentTitle);
		title.setText(mTitulo);
		ProgressBar pb = (ProgressBar)v.findViewById(R.id.progressBar1);
		mSearchView = (SearchView)v.findViewById(R.id.search_view);
		mListView = (ListView)v.findViewById(android.R.id.list);
		mListView.setTextFilterEnabled(true);
	    mAdapter = new ShopListAdapter(getActivity(),android.R.id.list,mOperadores,pb);
	    mListView.setAdapter(mAdapter);
		 setupSearchView();
		 if(mOperadores.size() == 0) {
				pb.setVisibility(View.INVISIBLE);
				if(getActivity()!=null) {
					Toast toast = Toast.makeText(getActivity().getApplicationContext(), "No hay tiendas en esta categoría", Toast.LENGTH_SHORT);
					toast.show();
				}
			} 
		
		return v;
	}

	 private void setupSearchView() {
	        mSearchView.setIconifiedByDefault(false);
	        mSearchView.setOnQueryTextListener(this);
	        mSearchView.setSubmitButtonEnabled(true);
	        mSearchView.setQueryHint("Buscar una tienda");
	    }

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		mSearchView.clearFocus();
		Operador operadorInfo = mOperadores.get(position);
		if (isMap) {
			((MainActivity)getActivity()).openShopMap(operadorInfo);
		} else {
			((MainActivity)getActivity()).openDetail(operadorInfo);
		}
	}



	@Override
	public boolean onQueryTextChange(String newText) {
		 if (TextUtils.isEmpty(newText)) {
			 	mAdapter.getFilter().filter("");
	            mListView.clearTextFilter();
	        } else {
	        	mAdapter.getFilter().filter(newText.toString());
	        }
	        return true;
	}



	@Override
	public boolean onQueryTextSubmit(String arg0) {
		mSearchView.clearFocus();
		return true;
	}
	
	private class ShopListAdapter extends ArrayAdapter<Operador> implements Filterable {
		
		private Context context;
		
		private List<Operador> tiendas;
		private List<Operador> origData;
		private ProgressBar mPb;
		
		public ShopListAdapter (Context context, int resource,  List<Operador> tiendas, ProgressBar pb) {
			super(context, resource, tiendas);
			this.context = context;
			this.tiendas = tiendas;
			this.mPb = pb;
			this.origData = new ArrayList<Operador>(this.tiendas);
		}
		

		@Override
		public int getCount() {
			return tiendas.size();
		}

		@Override
		public Operador getItem(int position) {
			Operador tienda = tiendas.get(position);
			return tienda;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			mPb.setVisibility(View.VISIBLE);
			if(convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(R.layout.shop_cell, null);
			}
			Operador tienda = getItem(position);
			ImageView icon = (ImageView)convertView.findViewById(R.id.shop_icon);
			if(tienda.getLogo_bitmap() != null) {
			 icon.setImageBitmap(BitmapFactory.decodeByteArray(tienda.getLogo_bitmap(), 0, tienda.getLogo_bitmap().length));
			} else {
				icon.setImageURI(Uri.parse(tienda.getLogo()));
			}
			TextView nombre = (TextView)convertView.findViewById(R.id.shop_name);
			nombre.setText(tienda.getNombre());
			//if(position == tiendas.size()-1) mPb.setVisibility(View.GONE);
		 mPb.setVisibility(View.GONE);  //mmr
			return convertView;
		}

		@Override
		public Filter getFilter() {
			return new Filter () {

				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					constraint = constraint.toString().toLowerCase();
					FilterResults result = new FilterResults();
					
					if(constraint != null && constraint.toString().length() > 0) {
						List<Operador> founded =  new ArrayList<Operador>();
						for(Operador  item:origData) {
							if((item.getNombre()).toLowerCase().contains(constraint)) {
								founded.add(item);
							}
						}
						result.values = founded;
						result.count = founded.size();
					} else {
						result.values = origData;
						result.count = origData.size();
					}
					return result;
				}

				@SuppressWarnings("unchecked")
				@Override
				protected void publishResults(CharSequence constraint,
						FilterResults results) {
					clear();
					for (Operador item: (List<Operador>) results.values) {
						add(item);
					}
					notifyDataSetChanged();					
				}
				
			};
		}
		
		
		
	}
	
	
	

}
