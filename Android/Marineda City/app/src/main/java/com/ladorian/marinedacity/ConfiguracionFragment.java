package com.ladorian.marinedacity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ConfiguracionFragment extends Fragment {
	private SharedPreferences prefs;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.configuracion_fragment, null);
		Button firstBtn = (Button)v.findViewById(R.id.firsBtn);
		Button secondBtn = (Button)v.findViewById(R.id.secondBtn);
		firstBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainActivity)getActivity()).openCardForm();
				
			}
		});
		if(isRegistered()) {
			TextView title = (TextView)v.findViewById(R.id.configTitle);
			title.setText("¡Bienvenido!\n¿Deseas modificar tus datos?");
			firstBtn.setText("MODIFICAR DATOS");
			secondBtn.setVisibility(View.GONE);
			String cityState = "";
			if(prefs.getString("estadoCity", null).equals("-1")) {
				cityState = "No Solicitada";
			} else if (prefs.getString("estadoCity", null).equals("0")) {
				cityState = "En Proceso";
			} else if  (prefs.getString("estadoCity", null).equals("1")){
				cityState = prefs.getString("numeroCity",null);
			}
			TextView userDesc = (TextView)v.findViewById(R.id.userDescription);
			String userData = "Nombre: "+prefs.getString("nombre", null)+" "+prefs.getString("apellidos", null)+"\n"
							+ "Email: " +prefs.getString("email", null)+"\n"
							+"Intereses: "+prefs.getString("intereses", null)+"\n"
							+"City Card: "+cityState;
			userDesc.setText(userData);
			//Faltaría sustituir la descripción por los datos de usuario
		}
		return v;
	}
	
	private boolean isRegistered() {
		prefs = getActivity().getSharedPreferences("userData", Context.MODE_PRIVATE);
		if(prefs !=null && prefs.getString("nombre",null) != null) return true;
		return false;
	}
	
}
