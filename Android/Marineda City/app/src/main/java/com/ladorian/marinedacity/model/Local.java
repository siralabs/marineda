package com.ladorian.marinedacity.model;

public class Local {
	int id;
	String nombre;
	String planta;
	String latitud;
	String longitud;
	int id_operador;
	float distancia;
	
	
	public Local() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPlanta() {
		return planta;
	}
	public void setPlanta(String planta) {
		this.planta = planta;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public int getId_operador() {
		return id_operador;
	}
	public void setId_operador(int id_operador) {
		this.id_operador = id_operador;
	}
	public float getDistancia() {
		return distancia;
	}
	public void setDistancia(float distancia) {
		this.distancia = distancia;
	}	
	
	
}
