package com.ladorian.marinedacity;

import com.ladorian.marinedacity.helper.DownloadImageTask;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

public final class PublicidadFragment extends Fragment {
	int position;
	PubliFragmentAdapter mAdapter;
    public PublicidadFragment (int i,PubliFragmentAdapter adapter) {
    	position = i;
        mAdapter = adapter;
    }
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		//ImageView image = new ImageView(getActivity());
		View rootView = inflater.inflate(R.layout.publi_fragment, container,false);
		ImageView image = (ImageView) rootView.findViewById(R.id.publiImage);
		if(mAdapter.data.size()>0) {
			new DownloadImageTask(image)
			.execute(mAdapter.data.get(position));
		}
        //image.setImageResource(imageResourceId);
        /*LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.CENTER);
        layout.addView(image);
        Log.i("MARINEDA",mAdapter.data.toString());
        return layout;*/
		
		return rootView;
	}

}
