package com.ladorian.marinedacity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PubliPagerActivity extends FragmentActivity {
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.publiPager);
		setContentView(mViewPager);
		
		FragmentManager fm = getSupportFragmentManager();
		mViewPager.setAdapter(new FragmentStatePagerAdapter (fm) {
			private int[] Images = new int[] {R.drawable.publi1,R.drawable.publi2};
			private int mCount = Images.length;
			@Override
			public Fragment getItem(int position) {
				return new PubliFragment(Images[position]);
			}

			@Override
			public int getCount() {
				return mCount;
			}
			
		});
	}
	
	
}
