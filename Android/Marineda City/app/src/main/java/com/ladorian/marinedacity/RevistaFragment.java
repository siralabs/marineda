package com.ladorian.marinedacity;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RevistaFragment extends Fragment implements OnPageChangeListener {
 
	PDFView pdfview;
	Integer pageNumber = 1;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.revista_container, null);
		pdfview = (PDFView) v.findViewById(R.id.pdfview);
		
		return v;
	}
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		
        //setTitle(pdfName = assetFileName);
        
		pdfview.fromAsset("blue.pdf")
		//.pages(0, 2, 1, 3, 3, 3)
		//.defaultPage(0)
		.showMinimap(false)
		.enableSwipe(true)
		//.onDraw(onDrawListener)
		//.onLoad(onLoadCompleteListener)
		.onPageChange(this)
		.load();
	}
	
	@Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        
    }
	
	

}
