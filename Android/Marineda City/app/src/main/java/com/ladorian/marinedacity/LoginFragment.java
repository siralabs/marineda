package com.ladorian.marinedacity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class LoginFragment extends Fragment implements ValidationListener,OnEditorActionListener {
	private Validator validator;
	@Required(order = 1,message="Introduzca un email")
	@Email(order = 2,message="Email no válido")
	private EditText emailText;
	@Password(order=3,message="Introduzca su contraseña")
	@TextRule(order=4,minLength=5,message="Mín. de 5 caracteres")
	private EditText password;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		validator = new Validator(this);
		validator.setValidationListener(this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.login_form, null);
		v.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hideKeyboard(v);
				return false;
			}
			
		});
		emailText = (EditText)v.findViewById(R.id.editText1);
		emailText.setOnEditorActionListener(this);
		password = (EditText)v.findViewById(R.id.editText2);
		password.setOnEditorActionListener(this);
		Button loginBtn = (Button)v.findViewById(R.id.loginBtn);
		loginBtn.setOnClickListener(new OnClickListener () {

			@Override
			public void onClick(View v) {
				validator.validate();				
			}
			
		});
		return v;
	}
	
	@Override
	public void onValidationSucceeded() {
		FragmentManager fm = getActivity().getSupportFragmentManager();
		final WaitingDialog wd = new WaitingDialog();
		wd.show(fm, "waiting");
		LoginData loginData = new LoginData(emailText.getText().toString(),password.getText().toString());
		ApiClient.getMarinedaCityApiClient().loginUser(loginData, new Callback<Response> () {

			@Override
			public void failure(RetrofitError arg0) {
				 if(arg0 == null) { 
					   Log.d("ApiFail","Error");
					   }
				 else {
					 	Log.d("ApiFail",arg0.getBody().toString());
					   }
				 wd.dismiss();
				 failAlert(null);
				
			}

			@Override
			public void success(Response arg0, Response arg1) {
				TypedInput body = arg0.getBody();
				try {
					BufferedReader reader = new BufferedReader (new InputStreamReader(body.in()));
					StringBuilder out = new StringBuilder();
					String newLine = System.getProperty("line.separator");
					String line;
					while((line = reader.readLine()) != null) {
						out.append(line);
						out.append(newLine);
					}
					JSONObject obj = new JSONObject(out.toString());						
					Log.d("ApiSuccess",obj.getString("message"));
					boolean error = Boolean.parseBoolean(obj.getString("error"));
					if(!error) {
						wd.dismiss();
						saveUserData(obj);	
					} else {
						wd.dismiss();
						failAlert(obj.getString("message"));
					}
					wd.dismiss();
				} catch (IOException e) {
					e.printStackTrace();
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		});
		
	}
	
	public void saveUserData (JSONObject data) {
		SharedPreferences prefs = getActivity().getSharedPreferences("userData", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		JSONObject user;
		JSONArray intereses;
		try {
			user = data.getJSONObject("user");
			intereses = user.getJSONArray("intereses");
			List<String> list = new ArrayList<String>();
			for(int i = 0; i<intereses.length();i++) {
				list.add(intereses.getString(i));
			}
			String interesesString = TextUtils.join(", ",list);
			editor.putString("nombre", user.getString("nombre"));
			editor.putString("apellidos", user.getString("apellidos"));
			editor.putString("intereses",interesesString);
			editor.putString("telefono", user.getString("telefono"));
			editor.putString("email",user.getString("email"));
			editor.putString("password", password.getText().toString());
			editor.putString("dni",user.getString("dni"));
			editor.putString("nacimiento", user.getString("nacimiento"));
			editor.putString("estadoCity", user.getString("estadoCity"));
			editor.putString("numeroCity", user.getString("numeroCity"));
			editor.commit();
		} catch (JSONException e) {
			e.printStackTrace();
			failAlert(null);
		}
		((MainActivity)getActivity()).onBackPressed();
		
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		 String message = failedRule.getFailureMessage();

	        if (failedView instanceof EditText) {
	            failedView.requestFocus();
	            ((EditText) failedView).setError(message);
	        } else {
	            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	        }
		
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
			v.clearFocus();
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
           return true;
        }
        return false;
	   }
	
	protected void hideKeyboard(View view) {
		 InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		 in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	public void failAlert(String message) {
		String msg = "No se ha podido completar el registro. Inténtelo más tarde.";
		if(message != null) msg = message;
		new AlertDialog.Builder(getActivity())
	    .setTitle("Fallo en el registro")
	    .setMessage(msg)
	    .setPositiveButton("Aceptar", null)
	    .show();
	}
}
