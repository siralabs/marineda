package com.ladorian.marinedacity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

public class RedesSocialesFragment extends Fragment implements OnItemClickListener  {
	private final RedesListAdapter mAdapter = new RedesListAdapter();
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.redes_sociales,null);
		ListView lv = (ListView)v.findViewById(R.id.redes_list);
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(this);
		return v;
	}
	
	private class RedesListAdapter extends BaseAdapter {
		private final List<Map<String,Object>> redes = new ArrayList<Map<String,Object>> ();
		public RedesListAdapter () {
			super();
			Map <String,Object> red0 = new HashMap<String,Object>();
			red0.put("logo", R.drawable.logo_facebook);
			red0.put("url", "https://www.facebook.com/centrocomercialmarinedacity?fref=ts");
			redes.add(red0);
			Map <String,Object> red1 = new HashMap<String,Object>();
			red1.put("logo", R.drawable.logo_twitter);
			red1.put("url", "https://twitter.com/marinedacity");
			redes.add(red1);
			Map <String,Object> red2 = new HashMap<String,Object>();
			red2.put("logo", R.drawable.logo_tuenti);
			red2.put("url", "https://www.tuenti.com/#m=Profile&func=index&user_id=75703187");
			redes.add(red2);
			Map <String,Object> red3 = new HashMap<String,Object>();
			red3.put("logo", R.drawable.logo_youtube);
			red3.put("url", "https://www.youtube.com/channel/UCOa3YlWwtg--DabErAKEIeg");
			redes.add(red3);
			Map <String,Object> red4 = new HashMap<String,Object>();
			red4.put("logo", R.drawable.logo_instagram);
			red4.put("url", "http://instagram.com/");
			redes.add(red4);
			Map <String,Object> red5 = new HashMap<String,Object>();
			red5.put("logo", R.drawable.logo_pinterest);
			red5.put("url", "http://es.pinterest.com/marinedacity/");
			redes.add(red5);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return redes.size();
		}

		@Override
		public Map<String,Object> getItem(int position) {
			Map<String,Object> red = redes.get(position);
			return red;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(R.layout.redes_cell, null);
			}			 
			ImageView logo = (ImageView)convertView.findViewById(R.id.logo);
			logo.setImageResource((Integer) getItem(position).get("logo"));
			return convertView;
		}
		
		
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		String url = (String)mAdapter.getItem(position).get("url");
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
		
	}

}
