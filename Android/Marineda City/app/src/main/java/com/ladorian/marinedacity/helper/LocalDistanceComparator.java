package com.ladorian.marinedacity.helper;

import java.util.Comparator;

import com.ladorian.marinedacity.model.Local;

public class LocalDistanceComparator implements Comparator<Local> {

	@Override
	public int compare(Local loc1, Local loc2) {
		return loc1.getDistancia() < loc2.getDistancia() ? -1 
			     : loc1.getDistancia() > loc2.getDistancia() ? 1 
			     : 0;
	}

}
