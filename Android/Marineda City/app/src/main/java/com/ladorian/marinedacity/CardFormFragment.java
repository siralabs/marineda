package com.ladorian.marinedacity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

import com.ladorian.marinedacity.parkingDialogFragment.NoticeDialogListener;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Regex;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class CardFormFragment extends Fragment implements OnDateSetListener,OnClickListener,ValidationListener,OnEditorActionListener {
	
	
	private Validator validator;
	private static final String[] mIntereses = Constants.INTERESES_STRING;
	@Required(order = 1,message="Seleccione sus intereses")
	private EditText intereses;
	@Required(order = 2,message="Introduzca su nombre")
	private EditText nombre;
	@Required(order = 3,message="Introduzca sus apellidos")
	private EditText apellidos;
	@Required(order = 4,message="Seleccione una fecha")
	private EditText nacimiento;
	@Required(order=5,message ="Introduzca su DNI")
	@Regex(order=6,pattern="(([X-Z]{1})([-]?)(\\d{7})([-]?)([A-Z]{1}))|((\\d{8})([-]?)([A-Z]{1}))",message="DNI no válido")
	private EditText dni;
	@Required(order=7,message="Introduzca un teléfono")
	@Regex(order=8,pattern="^[0-9]{9}?",message="Número no válido")
	private EditText telefono;
	@Required(order = 9,message="Introduzca un email")
	@Email(order = 10,message="Email no válido")
	private EditText emailText;
	@Password(order=11)
	@TextRule(order=12,minLength=5,message="Mín. de 5 caracteres")
	private EditText password;
	@ConfirmPassword(order=13,message="La contraseña no coincide")
	private EditText repassword;
	private Button submitBtn;
	private String estadoCity = "0";
	private final ArrayList<String> interesesToSend = new ArrayList<String> ();
	private SharedPreferences prefs;
	private boolean isEditing = false;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		prefs = getActivity().getSharedPreferences("userData",Context.MODE_PRIVATE);
		View v = inflater.inflate(R.layout.card_form, null);
		LinearLayout layout = (LinearLayout)v.findViewById(R.id.cardForm);
		layout.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hideKeyboard(v);
				return false;
			}
			
		});
		ImageButton closeButton = (ImageButton)v.findViewById(R.id.closebtn);
		closeButton.setOnClickListener(new View.OnClickListener(){
	    	@Override
	    	public void onClick(View v) {
	    		((MainActivity)getActivity()).onBackPressed();
	    	}
	    	});
		final ScrollView scrollview = ((ScrollView) v.findViewById(R.id.scrollView1)); scrollview.post(new Runnable() {
			  @Override public void run() {
			    scrollview.fullScroll(ScrollView.FOCUS_UP);
			  }
			});
		scrollview.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hideKeyboard(v);
				return false;
			}
			
		});
		nombre = (EditText)v.findViewById(R.id.editText2);
		nombre.setOnEditorActionListener(this);
		apellidos = (EditText)v.findViewById(R.id.editText3);
		apellidos.setOnEditorActionListener(this);
		nacimiento = (EditText)v.findViewById(R.id.editText4);
		nacimiento.setOnClickListener(this);
		dni = (EditText)v.findViewById(R.id.editText5);
		dni.setOnEditorActionListener(this);
		telefono = (EditText)v.findViewById(R.id.editText6);
		telefono.setOnEditorActionListener(this);
		intereses = (EditText)v.findViewById(R.id.editText1);
		intereses.setOnClickListener(this);
		emailText = (EditText)v.findViewById(R.id.editText7);
		emailText.setOnEditorActionListener(this);
		password = (EditText)v.findViewById(R.id.editText8);
		password.setOnEditorActionListener(this);
		repassword = (EditText)v.findViewById(R.id.editText9);
		repassword.setOnEditorActionListener(this);
		submitBtn = (Button)v.findViewById(R.id.submitBtn);
		Switch citySwitch = (Switch)v.findViewById(R.id.estadoCity);
		citySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				estadoCity = (isChecked)?"0":"-1";
				if(isChecked) {
					submitBtn.setText("CREAR CUENTA Y SOLICITAR LA CITY CARD");
				} else {
					submitBtn.setText("CREAR CUENTA SIN LA CITY CARD");
				}
			}
		});
		
		if(prefs != null && prefs.getString("nombre",null) != null) {
			isEditing = true;
			nombre.setText(prefs.getString("nombre", null));
			
			if(prefs.getString("estadoCity", null).equals("0")) {
				citySwitch.setChecked(true);
			} else if (prefs.getString("estadoCity", null).equals("-1")) {
				citySwitch.setChecked(false);
			} else { citySwitch.setClickable(false);}
			estadoCity = prefs.getString("estadoCity", null);
			apellidos.setText(prefs.getString("apellidos",null));
			nacimiento.setText(prefs.getString("nacimiento",null));
			dni.setText(prefs.getString("dni", null));
			telefono.setText(prefs.getString("telefono", null));
			intereses.setText(prefs.getString("intereses", null));
			emailText.setText(prefs.getString("email",null));
			emailText.setFocusable(false);
			password.setText(prefs.getString("password",null));
			repassword.setText(prefs.getString("password", null));
			password.setFocusable(false);
			repassword.setFocusable(false);
			submitBtn.setText("MODIFICAR DATOS");
		}
		
		Button registerBtn = (Button)v.findViewById(R.id.submitBtn);
		registerBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				validator.validate();
				
			}
			
		});
		return v;
	}
	
	private class DatePickerDialogFragment extends DialogFragment {
		
		private Fragment mFragment;
		private int mViewId;
		private final ArrayList<String> checkedIntereses = new ArrayList<String>();
		DatePickerDialogFragment (Fragment callback,int viewId) {
			mFragment = callback;
			mViewId = viewId;
		}
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			if(mViewId == R.id.editText4) {
				Calendar mcurrentDate=Calendar.getInstance();
	        	int mYear=mcurrentDate.get(Calendar.YEAR);
	        	int mMonth=mcurrentDate.get(Calendar.MONTH);
	        	int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);
	        	return new DatePickerDialog(getActivity(),(OnDateSetListener)mFragment,mYear,mMonth,mDay);
			}  else {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Selecciona tus intereses")
				.setMultiChoiceItems(mIntereses, new boolean [] {false, false,false,false,false}, new DialogInterface.OnMultiChoiceClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						if(isChecked) {
							if(!checkedIntereses.contains(mIntereses[which]))
								checkedIntereses.add(mIntereses[which]);
						} else {
							if(checkedIntereses.contains(mIntereses[which]))
								checkedIntereses.remove(mIntereses[which]);
						}
						
					}
				})
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						String interesesString = TextUtils.join(", ",checkedIntereses);
						intereses.setText(interesesString);
						interesesToSend.clear();
						interesesToSend.addAll(checkedIntereses);
					}
				});
				
				return builder.create();
			}
			
		}
			
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		nacimiento.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
		
	}

	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.editText4 || v.getId() == R.id.editText1) {
        	FragmentTransaction ft = getFragmentManager().beginTransaction();
	        DatePickerDialogFragment newFragment = new DatePickerDialogFragment(this,v.getId());
	        newFragment.show(ft,"picker");
		}
		
	}

	@Override
	public void onValidationSucceeded() {
		FragmentManager fm = getActivity().getSupportFragmentManager();
		final WaitingDialog wd = new WaitingDialog();
		wd.show(fm, "waiting");
		ArrayList<String> interesesList = new ArrayList<String>(Arrays.asList(intereses.getText().toString().split("\\s*,\\s*")));
		User userData = new User(emailText.getText().toString(),nombre.getText().toString(),apellidos.getText().toString(),telefono.getText().toString(),dni.getText().toString(),interesesList,nacimiento.getText().toString(),password.getText().toString(),estadoCity);
		if(!isEditing) {
			ApiClient.getMarinedaCityApiClient().registerUser(userData, new Callback<Response> () {
				@Override
				public void failure(RetrofitError arg0) {
					 if(arg0 == null) { 
						   Log.d("ApiFail","Error");
						   }
					 else {
						 	Log.d("ApiFail",arg0.getBody().toString());
						   }
					 wd.dismiss();
					 failAlert(null);
					
				}
	
				@Override
				public void success(Response arg0, Response arg1) {
					
					TypedInput body = arg0.getBody();
					try {
						BufferedReader reader = new BufferedReader (new InputStreamReader(body.in()));
						StringBuilder out = new StringBuilder();
						String newLine = System.getProperty("line.separator");
						String line;
						while((line = reader.readLine()) != null) {
							out.append(line);
							out.append(newLine);
						}
						JSONObject obj = new JSONObject(out.toString());						
						Log.d("ApiSuccess",obj.getString("message"));
						boolean error = Boolean.parseBoolean(obj.getString("error"));
						if(!error) {
							wd.dismiss();
							saveUserData();	
						} else {
							wd.dismiss();
							failAlert(obj.getString("message"));
						}
						wd.dismiss();
					} catch (IOException e) {
						e.printStackTrace();
					}
					catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			});
		} else {
			ApiClient.getMarinedaCityApiClient().updateUser(userData, new Callback<Response> () {

				@Override
				public void failure(RetrofitError arg0) {
					 if(arg0 == null) { 
						   Log.d("ApiFail","Error");
						   }
					 else {
						 	Log.d("ApiFail",arg0.getBody().toString());
						   }
					 wd.dismiss();
					 failAlert(null);
					
				}
	
				@Override
				public void success(Response arg0, Response arg1) {
					
					TypedInput body = arg0.getBody();
					try {
						BufferedReader reader = new BufferedReader (new InputStreamReader(body.in()));
						StringBuilder out = new StringBuilder();
						String newLine = System.getProperty("line.separator");
						String line;
						while((line = reader.readLine()) != null) {
							out.append(line);
							out.append(newLine);
						}
						JSONObject obj = new JSONObject(out.toString());						
						Log.d("ApiSuccess",obj.getString("message"));
						boolean error = Boolean.parseBoolean(obj.getString("error"));
						if(!error) {
							wd.dismiss();
							saveUserData();	
						} else {
							wd.dismiss();
							failAlert(obj.getString("message"));
						}
						wd.dismiss();
					} catch (IOException e) {
						e.printStackTrace();
					}
					catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			});
		}
	}
	
	public void saveUserData () {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("nombre", nombre.getText().toString());
		editor.putString("apellidos", apellidos.getText().toString());
		editor.putString("intereses", intereses.getText().toString());
		editor.putString("telefono", telefono.getText().toString());
		editor.putString("email",emailText.getText().toString());
		editor.putString("password", password.getText().toString());
		editor.putString("dni",dni.getText().toString());
		editor.putString("nacimiento", nacimiento.getText().toString());
		editor.putString("estadoCity", estadoCity);
		editor.commit();
		((MainActivity)getActivity()).onBackPressed();
	}
	
	public void failAlert(String message) {
		String msg = "No se ha podido completar el registro. Inténtelo más tarde.";
		if(message != null) msg = message;
		new AlertDialog.Builder(getActivity())
	    .setTitle("Fallo en el registro")
	    .setMessage(msg)
	    .setPositiveButton("Aceptar", null)
	    .show();
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		 String message = failedRule.getFailureMessage();

	        if (failedView instanceof EditText) {
	            failedView.requestFocus();
	            ((EditText) failedView).setError(message);
	        } else {
	            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	        }
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		validator = new Validator(this);
		validator.setValidationListener(this);
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
			v.clearFocus();
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
           return true;
        }
        return false;
	   }
	
	protected void hideKeyboard(View view) {
		 InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		 in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	

}
