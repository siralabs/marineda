package com.ladorian.marinedacity;

import com.ladorian.marinedacity.model.Evento;
import com.ladorian.marinedacity.model.Oferta;
import com.ladorian.marinedacity.model.Operador;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;


import retrofit.Callback;
import retrofit.RestAdapter;

import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;


public class ApiClient {
	public static MarinedaCityApiInterface sMarinedaWS;
	
	public static MarinedaCityApiInterface getMarinedaCityApiClient() {
		if(sMarinedaWS == null) {
			RestAdapter restAdapter = new RestAdapter.Builder()
						.setLogLevel(RestAdapter.LogLevel.FULL)
						.setEndpoint(Constants.WEBSERVICEURL)
						.build();
			
			sMarinedaWS = restAdapter.create(MarinedaCityApiInterface.class);
		}
		
		return sMarinedaWS;
	}
	
	public interface MarinedaCityApiInterface {
		@GET(Constants.GET_OPERADORES)
		void getOperadores(Callback<List<Operador>> callback);
		
		@POST(Constants.REGISTER_USER)
		void registerUser(@Body User userData, Callback<Response> callback);
		
		@POST(Constants.UPDATE_USER)
		void updateUser(@Body User userData, Callback<Response> callback);
		
		@POST(Constants.LOGIN_USER)
		void loginUser(@Body LoginData loginData, Callback<Response> callback);
		
		@GET(Constants.GET_EVENTOS)
		void getEventos (Callback<List<Evento>> callback);
		
		@GET(Constants.GET_OFERTAS)
		void getOfertas(Callback<List<Oferta>> callback);
		
		@GET(Constants.GET_CITY_OFERTAS)
		void getCityOfertas(Callback<List<Oferta>> callback);
		
		@GET(Constants.GET_OFERTAS+"/{id}")
		void getShopDeals(
				@Path("id") String shop_id,
				Callback<Map<String,Object>> callback);
		
		@GET(Constants.UPDATE_OPERADORES+"/{date}")
		void updateOperadores(
				@Path("date") String update,
				Callback<List<List<Operador>>> callback);
		
		@GET(Constants.GET_REVISTA)
		void getRevista(
				Callback <Map<String,Object>> callback);
	}
	
	
}

