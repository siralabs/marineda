package com.ladorian.marinedacity.helper;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	ImageView mImage;
	
	public DownloadImageTask (ImageView imageview) {
		this.mImage = imageview;
	}
	@Override
	protected Bitmap doInBackground(String... params) {
		Bitmap detailImage = null;
		String urldisplay = params[0];
		try {
			URL url = new URL(urldisplay);
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			urldisplay = uri.toASCIIString();
			InputStream in = new java.net.URL(urldisplay).openStream();
			detailImage = BitmapFactory.decodeStream(in);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return detailImage;
	}
	@Override
	protected void onPostExecute(Bitmap result) {
		mImage.setImageBitmap(result);
	}
	
	

}
