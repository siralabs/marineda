package com.ladorian.marinedacity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.bonuspack.overlays.MapEventsOverlay;
import org.osmdroid.bonuspack.overlays.MapEventsReceiver;
import org.osmdroid.tileprovider.MapTileProviderArray;
import org.osmdroid.tileprovider.modules.IArchiveFile;
import org.osmdroid.tileprovider.modules.MBTilesFileArchive;
import org.osmdroid.tileprovider.modules.MapTileFileArchiveProvider;
import org.osmdroid.tileprovider.modules.MapTileModuleProviderBase;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay;

import com.ladorian.marinedacity.helper.DatabaseHelper;
import com.ladorian.marinedacity.helper.LocalDistanceComparator;
import com.ladorian.marinedacity.model.Local;
import com.ladorian.marinedacity.model.Operador;

import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

public class ChildPlanoFragment extends Fragment implements MapEventsReceiver {
	private MapView planoView;
	private XYTileSource tSource;
	private DefaultResourceProxyImpl resProxy;
	private SimpleRegisterReceiver sr;
	private int mMap;
	private MapController mController;
	private Operador operador;
	private Local local;
	private ItemizedOverlay<OverlayItem> markerOverlay;
	private ArrayList<Local> tapLocales;
	
	ArrayList<OverlayItem> overlayItemArray;
	
	public ChildPlanoFragment(int map) {
		this.mMap = map;
	}
	

	public void setOperador(Operador operador) {
		this.operador = operador;
	}

	

	public void setLocal(Local local) {
		this.local = local;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StringBuilder filename =new StringBuilder();
		tapLocales = new ArrayList<Local>();
		switch (mMap) {
		case 0:
			filename.append("mnda0.mbtiles");
			break;
		case 1:
			filename.append("mnda1.mbtiles");
			break;
		case 2:
			filename.append("mnda2.mbtiles");
			break;
		}
		resProxy = new DefaultResourceProxyImpl(getActivity().getApplicationContext());		
		String [] baseUrl = {};
		tSource = new XYTileSource("mbtiles",ResourceProxy.string.offline_mode,17,21,256,".png",baseUrl);
		sr = new SimpleRegisterReceiver(getActivity());
		
		File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MARINEDACITY/",filename.toString());
		IArchiveFile[] files = {MBTilesFileArchive.getDatabaseFileArchive(f)};
		
		MapTileModuleProviderBase moduleProvider;
		moduleProvider = new MapTileFileArchiveProvider(sr,tSource,files);
		
		MapTileModuleProviderBase[] pBaseArray;
		pBaseArray = new MapTileModuleProviderBase[] {moduleProvider};
		
		MapTileProviderArray provider;
	    provider = new MapTileProviderArray(tSource, null, pBaseArray);
		
		planoView = new MapView(getActivity(),256,resProxy,provider);
		planoView.setBuiltInZoomControls(true);
		planoView.setMultiTouchControls(true);
		planoView.setBackgroundColor(android.R.color.white);
		
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.child_plano, null);
		planoView.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));
		ViewGroup group = (ViewGroup)v.findViewById(R.id.vistaMapa);
		group.addView(planoView);
		MapEventsOverlay overlayEventos = new MapEventsOverlay(getActivity().getBaseContext(),this);
		planoView.getOverlays().add(overlayEventos);
		planoView.invalidate();
		mController = (MapController)planoView.getController();
		mController.setZoom(18);
		GeoPoint point;
		StringBuilder longitud;
		StringBuilder latitud;
		if (local == null) {
			point = new GeoPoint(43.344328, -8.427522);
		} else {
			longitud = new StringBuilder(local.getLongitud());
			latitud = new StringBuilder(local.getLatitud());
			point = new GeoPoint(Double.parseDouble(latitud.toString()),Double.parseDouble(longitud.toString()));
		}
		mController.setCenter(point);
		if(local != null && operador !=null) {
			overlayItemArray = new ArrayList<OverlayItem>();
			OverlayItem shopMarker = new OverlayItem(operador.getNombre(),operador.getLocal(), point);
			overlayItemArray.add(shopMarker);
			this.markerOverlay = new ItemizedIconOverlay<OverlayItem>(getActivity(),overlayItemArray,null);
			planoView.getOverlays().add(this.markerOverlay);
			planoView.invalidate();
		}
		
		ViewTreeObserver vto = v.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onGlobalLayout() {
				IGeoPoint point;
				if (operador == null) {
					point = new GeoPoint(43.344328, -8.427522);
				} else {
					String longitud = local.getLongitud();
					String latitud = local.getLatitud();
					point = new GeoPoint(Double.parseDouble(latitud),Double.parseDouble(longitud));
				}
				mController.setCenter(point);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					planoView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				} else {
					planoView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				}				
			}
		});
		
		return v;
	}
	
	
	
	@Override
	public boolean longPressHelper(GeoPoint arg0) {
		
		return false;
	}



	@Override
	public boolean singleTapConfirmedHelper(GeoPoint arg0) {
		DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
		List<Local> locales = db.getLocalesPlanta(Integer.toString(mMap));
		db.close();
		moveToLocal(locales,arg0);
		//mController.animateTo(arg0);
		return false;
	}
	
	private void moveToLocal (List<Local> locales, GeoPoint point) {
			final double DEG_RATE = 1E6;
			tapLocales.clear();
		    double startLatitude = point.getLatitudeE6() / DEG_RATE;
		    double startLongitude= point.getLongitudeE6() / DEG_RATE;
		    Location loc1 = new Location("");
		    loc1.setLongitude(startLongitude);
		    loc1.setLatitude(startLatitude);
		for(Local local : locales) {
			Location loc2 = new Location("");
			loc2.setLatitude(Double.parseDouble(local.getLatitud()));
			loc2.setLongitude(Double.parseDouble(local.getLongitud()));
			
			float distance = loc1.distanceTo(loc2);
			local.setDistancia(distance);
			tapLocales.add(local);
		}
		Collections.sort(tapLocales,new LocalDistanceComparator());
		Log.d("LOCAL PRÓXIMO :", tapLocales.get(0).getNombre());
		if(tapLocales.get(0).getId_operador() != 0) {
			DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
			Operador operador = db.getOperador(tapLocales.get(0).getId_operador());
			db.closeDB();
			Toast.makeText(getActivity().getApplicationContext(), operador.getNombre(), Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(getActivity().getApplicationContext(),"LOCAL DISPONIBLE", Toast.LENGTH_SHORT).show();
		}
		
	}
	
}
