package com.ladorian.marinedacity;

/**
 * @name GCMPrivateKeys
 * @category private keys interface
 *
 * This class allows the developer to store those important data
 * necesary in SDK´s and GCM registration process
 *
 */

public interface GCMPrivateKeys {

    // APP_ID. GIVEN BY SEEKETING
    public final long APP_ID = 20142014133L;

    // SENDER_ID. API CONSOLE IDENTIFIER (OVERVIEW API CONSOLE)
    public final String SENDER_ID = "1065099229089";

    // SERVER_KEY. CREDENTIAL NEEDED IN ORDER TO USE GCM SERVICE (API CONSOLE)
    public final String SERVER_KEY = "AIzaSyBK-NE5fkB_WvPNO1WgDWakUp1oXsOJNak";

    // CONSTANTS NEEDED FOR NOTIFICATIONS CONTENT AND STRUCTURE

    public static final String KEY_ID = "notifId";
    public static final String MSG = "msg";
    public static final String ID = "id";
    public static final String TITLE = "We Observe!";
    public static final String SETS_INI_APP = "StartAppNoty";

}
