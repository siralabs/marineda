package com.ladorian.marinedacity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.ladorian.marinedacity.helper.GetCoordsAsyncTask;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Window;

public class FirstDownloadActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();
		setContentView(R.layout.first_download);
		GetCoordsAsyncTask getCoords = new GetCoordsAsyncTask(getApplicationContext(),this);
		getCoords.execute("");
		File resourcesDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MARINEDACITY/");
    	if(resourcesDir.mkdirs()) saveMaps();
	}

	@Override
	public void onBackPressed() {
	}
	
	private void saveMaps() {
		SharedPreferences prefs = this.getSharedPreferences("firstRun", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("isFirst", false);
	    AssetManager assetManager = getAssets();
	    String[] files = null;
	    try {
	        files = assetManager.list("");
	    } catch (IOException e) {
	        Log.e("tag", "Failed to get asset file list.", e);
	    }
	    for(String filename : files) {
	    	if(filename.equals("mnda0.mbtiles")||filename.equals("mnda1.mbtiles")||filename.equals("mnda2.mbtiles")) {
		        InputStream in = null;
		        OutputStream out = null;
		        try {
		          in = assetManager.open(filename);
		          File outFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MARINEDACITY/", filename);
		          out = new FileOutputStream(outFile);
		          copyFile(in, out);
		          in.close();
		          in = null;
		          out.flush();
		          out.close();
		          out = null;
		
		        } catch(IOException e) {
		            Log.e("tag", "Failed to copy asset file: " + filename, e);
		        }  
	    	}
	    	
	    }
	    finish();
	}
	
	private void copyFile(InputStream in, OutputStream out) throws IOException
	{
	    byte[] buffer = new byte[1024];
	    int read;
	    while ((read = in.read(buffer)) != -1)
	    {
	        out.write(buffer, 0, read);
	    }
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		android.os.Debug.stopMethodTracing();
	}
	
	
	
	
	
}
