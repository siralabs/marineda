package com.ladorian.marinedacity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class MyCityCardFragment extends Fragment {
	
	private boolean isMain;
	
	public MyCityCardFragment (boolean isMain) {
		this.isMain = isMain;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.card_view, null);
		SharedPreferences prefs = getActivity().getSharedPreferences("userData", Context.MODE_PRIVATE);
		if(prefs.getString("numeroCity", null) != null) {
			TextView nombre = (TextView)v.findViewById(R.id.nombre);
			nombre.setText(prefs.getString("nombre", null)+" "+prefs.getString("apellidos",null));
			TextView number = (TextView)v.findViewById(R.id.number);
			number.setText(prefs.getString("numeroCity",null));
		} else {
			showAlert();
		}
		ImageButton closeButton = (ImageButton)v.findViewById(R.id.closebtn);
		closeButton.setOnClickListener(new View.OnClickListener(){
	    	@Override
	    	public void onClick(View v) {
	    		//((MainActivity)getActivity()).closeDetail();
	    		((MainActivity)getActivity()).onBackPressed();
	    	}
	    	});
		if(isMain) closeButton.setVisibility(View.GONE);
		return v;
	}
	private void showAlert() {
		new AlertDialog.Builder(getActivity())
	    .setTitle("Tarjeta no encontrada.")
	    .setMessage("Su tarjeta no se ha solicitado o está pendiente de expedición.")
	    .setPositiveButton("Aceptar", null)
	    .show();
	}
	
}
