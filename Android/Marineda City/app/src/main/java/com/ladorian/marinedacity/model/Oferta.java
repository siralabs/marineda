package com.ladorian.marinedacity.model;

public class Oferta {
	int id;
	String descripcion;
	String detalle;
	String fin;
	String inicio;
	String imagen;
	String nombre;
	String idoperador;
	String operador;
	String miniatura;
	
	public Oferta () {
		
	}
	
	
	
	public String getIdoperador() {
		return idoperador;
	}


	public void setIdoperador(String idoperador) {
		this.idoperador = idoperador;
	}



	public String getMiniatura() {
		return miniatura;
	}



	public void setMiniatura(String miniatura) {
		this.miniatura = miniatura;
	}



	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getFin() {
		return fin;
	}
	public void setFin(String fin) {
		this.fin = fin;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	
	
}
