package com.ladorian.marinedacity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.ladorian.marinedacity.helper.ImageLoader;
import com.ladorian.marinedacity.model.Oferta;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class OfertasCityFragment extends Fragment implements OnItemClickListener {
	private SharedPreferences prefs;
	private int estadoSolicitud = 0;
	private ArrayList<Oferta> data = new ArrayList<Oferta> ();
	private OfertasCityAdapter mAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mAdapter = null;
	}
	
	


	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mAdapter = null;
	}



	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		mAdapter = new OfertasCityAdapter();
		View v = (View)inflater.inflate(R.layout.ofertas_marineda, null);
		prefs = (SharedPreferences)getActivity().getSharedPreferences("userData", Context.MODE_PRIVATE);
		Button verBtn = (Button)v.findViewById(R.id.verBtn);
		Button solicitarBtn = (Button)v.findViewById(R.id.solicitarBtn);
		if(prefs != null && prefs.getString("nombre",null)!=null) {
			if(prefs.getString("estadoCity", null).equals("-1")) {
				verBtn.setVisibility(View.GONE);
				solicitarBtn.setText("Solicítala ahora");
				estadoSolicitud = 1;
			} else if(prefs.getString("estadoCity", null).equals("0")) {
				verBtn.setVisibility(View.GONE);
				solicitarBtn.setText("Tarjeta pendiente de procesar");
				solicitarBtn.setClickable(false);
			} else {
				solicitarBtn.setVisibility(View.GONE);
				verBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						((MainActivity)getActivity()).openMyCityCard();
						
					}
					
				});
			}
			solicitarBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(estadoSolicitud == 0) {
						((MainActivity)getActivity()).openCardForm();
					} else {
						//SOLICITAR DIRECTAMENTE LA TARJETA
						SharedPreferences.Editor editor = prefs.edit();
						editor.putString("estadoCity", "0");
					}
					
				}
				
			});
		} else {
			solicitarBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
						((MainActivity)getActivity()).openCardForm();					
				}
				
			});
			verBtn.setVisibility(View.GONE);
		}
		ListView lv = (ListView)v.findViewById(android.R.id.list);
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(this);
		return v;
	}
	
	private class OfertasCityAdapter extends BaseAdapter {
		
		public ImageLoader imageLoader;
		
		public OfertasCityAdapter () {
			imageLoader = new ImageLoader(getActivity().getApplicationContext());
			ApiClient.getMarinedaCityApiClient().getCityOfertas(new Callback <List<Oferta>> (){
				   @Override
				   public void success(List<Oferta> ofertas, Response response) {
					   addOfertas(ofertas);
				   }			   
				   @Override
				   public void failure(RetrofitError retrofitError) {
					   if(retrofitError == null) { 
					   Log.d("Api","Error");
					   }
					   else {
						   Log.d("Api","Existe error");
					   }
				   }
			   });
		}

		@Override
		public int getCount() {
			
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			Oferta oferta = data.get(position);
			return oferta;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			System.out.println("getView " + position + " " + convertView);
			Oferta oferta = (Oferta) this.getItem(position);
			if(convertView==null) {
				LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.event_cell, null);
			}
			TextView nombre = (TextView)convertView.findViewById(R.id.event_name);
			nombre.setText(oferta.getNombre());
			ImageView image = (ImageView)convertView.findViewById(R.id.event_thumbnail);
			imageLoader.DisplayImage(oferta.getMiniatura(), image);
			return convertView;
		}
		
		public void addOfertas(List<Oferta> ofertas) {
			data.clear();
			data.addAll(ofertas);
			notifyDataSetChanged();
//			if (getActivity() != null) {
//				getActivity().runOnUiThread(new Runnable() {
//					public void run() {
//						notifyDataSetChanged();
//					}
//				});
//			}
			
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Oferta operadorInfo = (Oferta) mAdapter.getItem(position);
		((MainActivity)getActivity()).openEventDetail(operadorInfo);
		
	}


}
