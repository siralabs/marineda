package com.ladorian.marinedacity.model;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import com.ladorian.marinedacity.model.Local;

public class Operador {
	int id;
	int base_id;
	String categoria;
	String descripcion;
	String email;
	String imagen;
	String link;
	String local;
	String logo;
	String nombre;
	int favorito;
	String telefono;
	String firstLetter;
	byte[] logo_bitmap;
	
	private List<Local> locales = new ArrayList<Local>();
	
	public Operador() {
	}
	
	
	
	public List<Local> getLocales() {
		return locales;
	}

	public void setLocales(List<Local> locales) {
		this.locales = locales;
	}



	public byte[] getLogo_bitmap() {
		return logo_bitmap;
	}

	public void setLogo_bitmap(byte[] logo_bitmap) {
		this.logo_bitmap = logo_bitmap;
	}

	public int getBase_id() {
		return base_id;
	}
	public void setBase_id(int base_id) {
		this.base_id = base_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int isFavorito() {
		return favorito;
	}
	public void setFavorito(int favorito) {
		this.favorito = favorito;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFirstLetter() {
		return firstLetter;
	}
	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}
	
	
}
