package com.ladorian.marinedacity;

import android.app.NotificationManager;


import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.seeketing.sdks.refs.Refs;

/**
 * @name GCMIntentService
 * @category Intent service for GCM
 *
 * This class will configure all GCM service parameters
 *
 */
public class GCMIntentService extends GCMBaseIntentService implements
        GCMPrivateKeys {

    /**
     * @name GCMIntentService
     * @desc Class Constructor execution with proper sender id
     */

    public GCMIntentService() {
        super(SENDER_ID);
    }

    /**
     * @name onError
     * @desc If errors ocurred, this method will write error traces on log
     */

    @Override
    protected void onError(Context context, String errorId) {
        Log.d("GCM", "Error: " + errorId);
    }

    /**
     * @name onMesage
     * @desc defines message structure and its content
     */

    @Override
    protected void onMessage(Context context, Intent intent) {
        String msg = intent.getExtras().getString(MSG);
        String messageID = intent.getExtras().getString(ID);
        String title = intent.getExtras().getString(TITLE);

        if (title == null || title.equals("")) {
            title = context.getString(R.string.app_name);
        }

        mostrarNotificacion(title, msg, messageID);
    }

    /**
     * @name onRegistered
     * @desc Writes registration status OK on log
     */

    @Override
    protected void onRegistered(Context context, String regId) {
        Log.d("GCM", "onRegistered: Registrado OK.");
        Refs.registerForNotifications(regId);
    }

    /**
     * @name onRegistered
     * @desc Writes registration status NOK on log
     */

    @Override
    protected void onUnregistered(Context context, String regId) {
        Refs.registerForNotifications("");
    }

    /**
     * @name mostrarNotificacion (Show Notification)
     * @desc Sets the content of the notification, its ringtone
     * and ensures a smooth way back to the last activity known running.
     */

    private void mostrarNotificacion(String title, String msg, String messageID) {
        int notifId = 0;
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.marineda_logo);

        Uri defaultSound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.marineda_logo).setContentTitle(title)
                .setContentText(msg).setLargeIcon(largeIcon)
                .setSound(defaultSound).setAutoCancel(true);

        Intent resultIntent = new Intent(this, MainActivity.class);
        if (messageID != null && !messageID.equals("")) {
            notifId = Integer.valueOf(messageID);
            resultIntent.setData(Uri.parse(messageID));
            resultIntent.putExtra(KEY_ID, messageID);
        } else {
            resultIntent.putExtra(KEY_ID, 0);
            notifId = 0;
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(notifId, mBuilder.build());
    }

}