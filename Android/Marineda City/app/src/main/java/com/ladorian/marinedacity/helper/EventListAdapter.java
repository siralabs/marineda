package com.ladorian.marinedacity.helper;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.ladorian.marinedacity.ApiClient;
import com.ladorian.marinedacity.R;
import com.ladorian.marinedacity.model.Evento;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class EventListAdapter extends BaseAdapter {
	
	private Activity activity;
	private List<Evento> data;
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;
	ProgressBar pb;
	
	public EventListAdapter(Activity a, ProgressBar bar) {
		activity = a;
		pb = bar;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
		data = new ArrayList<Evento> ();
		ApiClient.getMarinedaCityApiClient().getEventos(new Callback <List<Evento>> (){
			   @Override
			   public void success(List<Evento> eventos, Response response) {
				   Log.d("Eventos",eventos.get(0).getNombre());
				   
				   addEventos(eventos);
				   //adapter.notifyDataSetChanged();
			   }			   
			   @Override
			   public void failure(RetrofitError retrofitError) {
				   if(retrofitError == null) { 
				   Log.d("Api","Error");
				   }
				   else {
					   Log.d("Api","Existe error");
				   }
			   }
		   });
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		Evento evento = data.get(position);
		return evento;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.event_cell, null);
		}
		Evento evento = (Evento) this.getItem(position);
		TextView nombre = (TextView) convertView.findViewById(R.id.event_name);
		nombre.setText(evento.getNombre());
		ImageView image = (ImageView) convertView.findViewById(R.id.event_thumbnail);
		imageLoader.DisplayImage(evento.getMiniatura(), image);

		return convertView;
	}
	
	public void addEventos(List<Evento> eventos) {
		this.data.clear();
		this.data.addAll(eventos);

		activity.runOnUiThread(new Runnable () {
			public void run() {
				pb.setVisibility(View.GONE);
				notifyDataSetChanged();
			}
		   });
		
	}

}
