package com.ladorian.marinedacity;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.ladorian.marinedacity.helper.DatabaseHelper;
import com.ladorian.marinedacity.helper.GetCoordsAsyncTask;
import com.ladorian.marinedacity.helper.UpdateOperadoresTask;
import com.ladorian.marinedacity.model.Evento;
import com.ladorian.marinedacity.model.Operador;
import com.seeketing.sdks.refs.Refs;
import com.seeketing.sdks.sets.Sets;
import com.seeketing.sdks.sets.UtilComms;

import net.sf.andpdf.pdfviewer.PdfViewerActivity;
import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.MenuDrawer.Type;
import net.simonvt.menudrawer.Position;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;
import android.app.ActionBar;

import com.google.android.gcm.GCMRegistrar;

public class MainActivity extends FragmentActivity
        implements parkingDialogFragment.NoticeDialogListener {

    private static final String JSON_PLANTA = "numPlanta";
    private static final String JSON_PLAZA = "numPlaza";
    private static final String JSON_COLOR = "color";
    private static final String JSON_LETRA = "letra";
    private static final String JSON_FECHA = "fecha";
    private static final String FILENAME = "parking.json";
    private String mPlanta;
    private String mPlaza;
    private String mColor;
    private String mLetra;
    private String mFecha;

    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    private MenuDrawer mDrawerLeft;
    private MenuDrawer mDrawerRight;
    private ViewPager mViewPager;
    private static Context mContext;
    PubliFragmentAdapter mAdapter;
    String dest_file_path = "revista.pdf";
    int downloadedSize = 0, totalsize;
    float per = 0;
    String notifId;
    private boolean pubWindow = false;
    private boolean started = true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //GetCoordsAsyncTask getCoords = new GetCoordsAsyncTask();
        //getCoords.execute("");
        mContext = this;
		Context context = getBaseContext();
		//****SEEKETING
		// START SESSION
        //Seeketing SDK Start Session
        this.pubWindow=false;
        Refs.startSession(this, true, true, GCMPrivateKeys.APP_ID, 0);

        //Track Event Text - StartApp
        if (started) {
            Sets.trackEventText("StartApp", "StartApp", "StartApp");
            started = false;
        }

        //Add an action to received notification
        Bundle i = getIntent().getExtras();
        int noti;
        if (i != null) {
            notifId = i.getString(GCMPrivateKeys.KEY_ID);
            if (notifId != null) {
                noti = Integer.valueOf(notifId);
                if (noti != 0) {
                    Sets.trackEventText("StartAppNoty", "NotyID=" + notifId, "StartAppNoty");
                    ViewGroup layout = (ViewGroup) findViewById(android.R.id.content);
                    this.pubWindow = Refs.showWindow(noti, (ViewGroup) layout,
                            null);
                }
            }
        }
        //long midevice=  Sets.getDeviceId();
		//*******SEEKETING
        // GCM SERVICE REGISTRATION
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        registerUser(getBaseContext());
        showActionBar();
        mDrawerLeft = MenuDrawer.attach(this, Type.OVERLAY, Position.LEFT, MenuDrawer.MENU_DRAG_WINDOW);
        mDrawerLeft.setMenuView(R.layout.menu_container);
        mDrawerRight = MenuDrawer.attach(this, Type.OVERLAY, Position.RIGHT, MenuDrawer.MENU_DRAG_WINDOW);
        mDrawerRight.setMenuView(R.layout.right_menu_container);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mainView = inflator.inflate(R.layout.activity_main, null);
        FragmentManager fm = getSupportFragmentManager();
        Fragment leftFragment = fm.findFragmentById(R.id.menuContainer);
        Fragment rightFragment = fm.findFragmentById(R.id.rightMenuContainer);
        mAdapter = new PubliFragmentAdapter(fm, this);
        mDrawerLeft.setContentView(mainView);
        mDrawerRight.setContentView(mainView);
        mViewPager = (ViewPager) mainView.findViewById(R.id.publiPager);
        mViewPager.setAdapter(mAdapter);
        if (leftFragment == null || rightFragment == null) {
            leftFragment = new LeftMenuFragment();
            rightFragment = new RightMenuFragment();
            fm.beginTransaction()
                    .add(R.id.menuContainer, leftFragment)
                    .add(R.id.rightMenuContainer, rightFragment)
                    .commit();
        }
        SharedPreferences prefs = this.getSharedPreferences("firstRun", Context.MODE_PRIVATE);
        if (prefs.getBoolean("isFirst", true)) {
            Intent intent = new Intent(this, FirstDownloadActivity.class);
            startActivity(intent);
        } else {
            UpdateOperadoresTask ut = new UpdateOperadoresTask(this.getApplicationContext());
            ut.execute();
        }
    }


    @SuppressLint("InflateParams")
    private void showActionBar() {
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.action_bar_title, null);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setCustomView(v);
        ImageButton leftBtn = (ImageButton) findViewById(R.id.btn1);
        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLeft.toggleMenu();
            }
        });
        ImageButton rightBtn = (ImageButton) findViewById(R.id.btn2);
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerRight.toggleMenu();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return false;
    }

    public void changeFragment(int groupPosition, int childPosition, String title, int menu) {
        mDrawerLeft.setContentView(R.layout.main_fragment_container);
        mDrawerRight.setContentView(R.layout.main_fragment_container);
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            // Get the back stack fragment id.
            int backStackId = getSupportFragmentManager().getBackStackEntryAt(i).getId();

            fm.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if (groupPosition < 2 && menu == 0) {
            SharedPreferences prefs = this.getSharedPreferences("firstRun", Context.MODE_PRIVATE);
            DatabaseHelper db = new DatabaseHelper(getApplicationContext());
            long opCount = db.countOperadores();
            db.closeDB();
            //if(prefs.getInt("totalOperadores", 0) == opCount) {
            ShopListFragment centralFragment = new ShopListFragment(title);
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
            //}

        } else if (groupPosition == 2 && menu == 0) {
            String url = "http://www.cinesa.es/Cines/Marineda-City";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (groupPosition == 3 && menu == 0) {

            EventListFragment centralFragment = new EventListFragment();
            centralFragment.setmTitulo(title);
            centralFragment.setmMode("evento");
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();

        } else if (groupPosition == 4 && menu == 0) {

            EventListFragment centralFragment = new EventListFragment();
            centralFragment.setmTitulo(title);
            centralFragment.setmMode("oferta");
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();

        } else if (groupPosition == 5 && menu == 0) {
            OfertasCityFragment centralFragment = new OfertasCityFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 6 && childPosition == 0 && menu == 0) {
            GuardarPosicionFragment centralFragment = new GuardarPosicionFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 6 && childPosition == 1 && menu == 0) {
            ParkingRecoveryFragment centralFragment = new ParkingRecoveryFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 7 && childPosition == 0 && menu == 0) {
            ShopListFragment centralFragment = new ShopListFragment(title, true);
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } /*else if(groupPosition == 8 && childPosition == 2 && menu == 0 ) {
            PlanoFragment centralFragment = new PlanoFragment(false);
			fm.beginTransaction()
			.replace(R.id.mainFragmentContainer, centralFragment)
			.commit();
		}*/ else if (groupPosition == 7 && childPosition == 1 && menu == 0) {
            PlanoFragment centralFragment = new PlanoFragment(false);
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 0 && childPosition == 0 && menu == 1) {
            InformacionFragment centralFragment = new InformacionFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 0 && childPosition == 1 && menu == 1) {

            ServiciosFragment centralFragment = new ServiciosFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 0 && childPosition == 2 && menu == 1) {
            ComoLLegarFragment centralFragment = new ComoLLegarFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 0 && childPosition == 3 && menu == 1) {
            MyCityCardFragment centralFragment = new MyCityCardFragment(true);
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 0 && childPosition == 4 && menu == 1) {
            MisFavoritosFragment centralFragment = new MisFavoritosFragment(this.getApplicationContext());
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        }/* else if (groupPosition == 0 && childPosition == 5 && menu == 1) {
 			RedesSocialesFragment centralFragment = new RedesSocialesFragment();
 			fm.beginTransaction()
 			.replace(R.id.mainFragmentContainer, centralFragment)
 			.commit();
 		}*/ else if (groupPosition == 0 && childPosition == 5 && menu == 1) {
            NewsletterFragment centralFragment = new NewsletterFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } /*else if (groupPosition == 0 && childPosition == 9 && menu ==1) {
 			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
 			emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 			emailIntent.setType("vnd.android.cursor.item/email");
 			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"opiniones@marinedacity.es"});
 			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Opinión desde la app Marineda City");
 			startActivity(Intent.createChooser(emailIntent, "Enviar correo usando..."));
 		}*/ else if (groupPosition == 0 && childPosition == 6 && menu == 1) {
            ConfiguracionFragment centralFragment = new ConfiguracionFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        } else if (groupPosition == 0 && childPosition == 7 && menu == 1) {
            AvisoLegalFragment centralFragment = new AvisoLegalFragment();
            fm.beginTransaction()
                    .replace(R.id.mainFragmentContainer, centralFragment)
                    .commit();
        }
        if (mDrawerLeft.isShown() && menu == 0) mDrawerLeft.toggleMenu();
        if (mDrawerRight.isShown() && menu == 1) mDrawerRight.toggleMenu();

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Matcher matcher = urlPattern.matcher(contents);
                if (matcher.find()) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(contents));
                    startActivity(i);
                }

            } else if (resultCode == RESULT_CANCELED) {
                // Handle cancel
                Log.i("App", "Scan unsuccessful");
            }
        }
    }

    public void openDetail(Operador operadorInfo) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //DetailFragment newFragment = (DetailFragment)getSupportFragmentManager().findFragmentById(R.id.detailContainer);
        DetailFragment newFragment = new DetailFragment();
        newFragment.setOperador(operadorInfo);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.addToBackStack(null);
        // Start the animated transition.
        ft.commit();
    }

    public void openShopMap(Operador operador) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        PlanoFragment newFragment = new PlanoFragment(true);
        newFragment.setmOperador(operador);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.addToBackStack(null);
        // Start the animated transition.
        ft.commit();
    }

    public void openEventDetail(Object eventInfo) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //DetailFragment newFragment = (DetailFragment)getSupportFragmentManager().findFragmentById(R.id.detailContainer);
        EventDetailFragment newFragment = new EventDetailFragment();
        newFragment.setData(eventInfo);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.addToBackStack(null);
        // Start the animated transition.
        ft.commit();
    }

    public void openShare(String title, String message, String image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        Uri path = Uri.fromFile(new File("android.resource://com.ladorian.marinedacity/drawable/agendathumbnail.jpg"));
        intent.putExtra(Intent.EXTRA_STREAM, path);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(intent, "send"));
    }

    public void openParking() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ParkingRecoveryFragment newFragment = new ParkingRecoveryFragment();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.commit();
    }

    public void openParada(String id, String title, String posx, String posy, Location myLocation) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ParadasMapFragment newFragment = new ParadasMapFragment();
        newFragment.setmId(id);
        newFragment.setmPosx(posx);
        newFragment.setmPosy(posy);
        newFragment.setmNombre(title);
        newFragment.setMyLocation(myLocation);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void openCardForm() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        CardFormFragment newFragment = new CardFormFragment();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.addToBackStack(null);
        ft.commit();

    }

    public void openOfertasCity() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        OfertasCityFragment newFragment = new OfertasCityFragment();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.commit();
    }

    public void openMyCityCard() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        MyCityCardFragment newFragment = new MyCityCardFragment(false);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.mainFragmentContainer, newFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        parkingDialogFragment plantaDialog = (parkingDialogFragment) dialog;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        mFecha = sdf.format(new Date());
        mPlanta = plantaDialog.mPlanta;
        mLetra = plantaDialog.mLetra;
        mColor = plantaDialog.mColor;
        mPlaza = plantaDialog.mPlaza;
        try {
            this.saveParking(this.toJSON());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
        this.openParking();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // TODO Auto-generated method stub

    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_PLANTA, mPlanta);
        json.put(JSON_PLAZA, mPlaza);
        json.put(JSON_LETRA, mLetra);
        json.put(JSON_COLOR, mColor);
        json.put(JSON_FECHA, mFecha);
        return json;
    }

    public void saveParking(JSONObject json) throws IOException {
        Writer writer = null;
        try {
            OutputStream out = mContext.openFileOutput(FILENAME, mContext.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(json.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    File downloadFile(String dwnload_file_path) {
        File file = null;
        try {

            URL url = new URL(dwnload_file_path);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            // connect
            urlConnection.connect();

            // set the path where we want to save the file
            File SDCardRoot = Environment.getExternalStorageDirectory();
            // create a new file, to save the downloaded file
            file = new File(SDCardRoot, dest_file_path);

            FileOutputStream fileOutput = new FileOutputStream(file);

            // Stream used for reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            // this is the total size of the file which we are
            // downloading
            totalsize = urlConnection.getContentLength();
            setText("Descargando la revista...");

            // create a buffer...
            byte[] buffer = new byte[1024 * 1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                per = ((float) downloadedSize / totalsize) * 100;
                setText("Tamaño total del PDF  : "
                        + (totalsize / 1024)
                        + " KB\n\nDescargando PDF " + (int) per
                        + "% completado");
            }
            // close the output stream when complete //
            fileOutput.close();
            setText("Descarga terminada. Abre una aplicación PDF de tu dispositivo.");

        } catch (final MalformedURLException e) {
            setTextError("Ha habido un error. Inténtalo más tarde.",
                    Color.RED);
        } catch (final IOException e) {
            setTextError("Ha habido un error. Inténtalo más tarde.",
                    Color.RED);
        } catch (final Exception e) {
            setTextError(
                    "Fallo en la descarga. Comprueba tu conexión a internet.",
                    Color.RED);
        }
        return file;
    }

    void setTextError(final String message, final int color) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
            }
        });

    }

    void setText(final String txt) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(mContext, txt, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * @param context The method needs the app context.
     * @name RegisterUser
     * @desc This method will execute GCM registration, registers #SenderID
     */

    private void registerUser(Context context) {
        final String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("")) {
            GCMRegistrar.register(context, SENDER_ID);
            Log.v("GCM", "Register OK");
        } else {
            Log.v("GCM", "Register NOK. Already registered.");
        }
    }

    /**
     * @name Register User
     * @desc We will take advantage of onDestroy method and its lifecycle within
     * the application to close refs session and stop communications between server
     * and app.
     */

    @Override
    public void onDestroy() {
        Sets.trackEventText("EndApp", "EndApp", "EndApp");
        Refs.endSession();
        super.onDestroy();

    }
}
