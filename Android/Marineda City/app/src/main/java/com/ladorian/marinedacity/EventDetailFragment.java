package com.ladorian.marinedacity;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.ladorian.marinedacity.helper.DatabaseHelper;
import com.ladorian.marinedacity.helper.DownloadImageTask;
import com.ladorian.marinedacity.model.Evento;
import com.ladorian.marinedacity.model.Oferta;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class EventDetailFragment extends Fragment implements OnCheckedChangeListener {
    public String mTitulo;
    public String mMode;
    public Object data;


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getmTitulo() {
        return mTitulo;
    }

    public void setmTitulo(String mTitulo) {
        this.mTitulo = mTitulo;
    }


    public String getmMode() {
        return mMode;
    }

    public void setmMode(String mMode) {
        this.mMode = mMode;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.event_detail_view, null);
        ImageButton closeButton = (ImageButton) v.findViewById(R.id.closebtn);
        TextView title = (TextView) v.findViewById(R.id.contentTitle);
        TextView nombre = (TextView) v.findViewById(R.id.nombre);
        TextView fecha = (TextView) v.findViewById(R.id.fecha);
        TextView detalle = (TextView) v.findViewById(R.id.detalle);
        TextView descripcion = (TextView) v.findViewById(R.id.descripcion);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date fechaInicio = null;
        Date fechaFin = null;
        StringBuilder inicio = new StringBuilder();
        StringBuilder fin = new StringBuilder();
        StringBuilder imageString = new StringBuilder();
        if (data instanceof Evento) {
            title.setText("EVENTO-" + ((Evento) getData()).getOperador());
            nombre.setText(((Evento) getData()).getNombre());
            inicio.append((((Evento) getData()).getInicio()));
            fin.append((((Evento) getData()).getFin()));
            detalle.setText(((Evento) getData()).getDetalle());
            descripcion.setText(((Evento) getData()).getDescripcion());
            imageString.append(((Evento) getData()).getImagen());
        } else if (data instanceof Oferta) {
            title.setText("OFERTA-" + ((Oferta) getData()).getOperador());
            nombre.setText(((Oferta) getData()).getNombre());
            inicio.append((((Oferta) getData()).getInicio()));
            fin.append((((Oferta) getData()).getFin()));
            detalle.setText(((Oferta) getData()).getDetalle());
            descripcion.setText(((Oferta) getData()).getDescripcion());
            imageString.append(((Oferta) getData()).getImagen());
        }
        try {
            fechaInicio = df.parse(inicio.toString());
            fechaFin = df.parse(fin.toString());
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM");
        String fechaInicio2 = df2.format(fechaInicio);
        String fechaFin2 = df2.format(fechaFin);
        fecha.setText("Del " + fechaInicio2 + " al " + fechaFin2);

        ImageView image = (ImageView) v.findViewById(R.id.detailImage);
        new DownloadImageTask(image)
                .execute(imageString.toString());

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity)getActivity()).closeDetail();
                ((MainActivity) getActivity()).onBackPressed();
            }
        });

        final SegmentedRadioGroup rdioGroup = (SegmentedRadioGroup) v.findViewById(R.id.segmentedcontrol);
        rdioGroup.clearCheck();
        rdioGroup.setOnCheckedChangeListener(this);


        return v;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        //group.clearCheck();
        int checkedBtn = group.getCheckedRadioButtonId();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date inicio = new Date();
        Date fin = new Date();
        if (checkedId == R.id.button_compartir && checkedBtn == checkedId) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TITLE, "MARINEDA CITY");
            if (data instanceof Evento) {
                intent.putExtra(Intent.EXTRA_SUBJECT, "EVENTO " + ((Evento) getData()).getNombre());
                intent.putExtra(Intent.EXTRA_TEXT, ((Evento) data).getNombre());
                try {
                    inicio = df.parse(((Evento) data).getInicio());
                    fin = df.parse(((Evento) data).getFin());
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            } else if (data instanceof Oferta) {
                intent.putExtra(Intent.EXTRA_SUBJECT, "OFERTA " + ((Oferta) getData()).getNombre());
                intent.putExtra(Intent.EXTRA_TEXT, ((Oferta) data).getNombre());
                try {
                    inicio = df.parse(((Oferta) data).getInicio());
                    fin = df.parse(((Oferta) data).getFin());
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            startActivity(Intent.createChooser(intent, "Compartir"));
            group.clearCheck();
        } else if (checkedId == R.id.button_calendario && checkedBtn == checkedId) {
            Calendar beginTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();
            try {
                if (data instanceof Evento) {
                    inicio = df.parse(((Evento) data).getInicio());
                    fin = df.parse(((Evento) data).getFin());
                } else if (data instanceof Oferta) {
                    inicio = df.parse(((Oferta) data).getInicio());
                    fin = df.parse(((Oferta) data).getFin());
                }
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            beginTime.setTime(inicio);
            endTime.setTime(fin);


            Intent intent = null;
            if (Build.VERSION.SDK_INT >= 14) {
                intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis());
                if (data instanceof Evento) {
                    intent.putExtra(Events.TITLE, "EVENTO " + ((Evento) getData()).getNombre());

                    intent.putExtra(Events.DESCRIPTION, ((Evento) data).getNombre());
                } else if (data instanceof Oferta) {
                    intent.putExtra(Events.TITLE, "OFERTA " + ((Oferta) getData()).getNombre());
                    intent.putExtra(Events.DESCRIPTION, ((Oferta) data).getNombre());
                }
                intent.putExtra(Events.EVENT_LOCATION, "Marineda City")
                        .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
            } else {
                intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", beginTime.getTimeInMillis());
                intent.putExtra("allDay", false);
                intent.putExtra("rrule", "FREQ=YEARLY");
                intent.putExtra("endTime", endTime.getTimeInMillis());
                if (data instanceof Evento) {
                    intent.putExtra("title", ((Evento) data).getNombre());
                } else if (data instanceof Oferta) {
                    intent.putExtra("title", ((Oferta) data).getNombre());
                }

            }
            if (intent.resolveActivity(this.getActivity().getPackageManager()) != null) {
                this.getActivity().startActivity(intent);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
                builder.setMessage("No hay calendarios instalados")
                        .setTitle("Evento no añadido");
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            group.clearCheck();
        } else if (checkedId == R.id.button_favorito && checkedBtn == checkedId) {
            DatabaseHelper db = new DatabaseHelper(getActivity());
            if (data instanceof Oferta) {
                db.createOferta((Oferta) data);
            } else if (data instanceof Evento) {
                db.createEvento((Evento) data);
            }
            db.closeDB();
            group.clearCheck();
        }

    }

}
