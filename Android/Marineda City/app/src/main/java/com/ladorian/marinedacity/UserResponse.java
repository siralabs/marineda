package com.ladorian.marinedacity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserResponse {
	private Boolean error;
	private Message message;

	public Boolean getError() {
	return error;
	}

	public void setError(Boolean error) {
	this.error = error;
	}

	String getMessage() {
	return message.toString();
	}

	public void setMessage(Message message) {
	this.message = message;
	}
	
	public String getNombre() {
		return this.message.getNombre();
	}


	private class Message {
		private String id;
		private String nombre;
		private String apellidos;
		private String email;
		private String dni;
		private String nacimiento;
		private String telefono;
		private String estadoCity;
		private Object numeroCity;
		private List<String> intereses = new ArrayList<String>();
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public String getId() {
		return id;
		}

		public void setId(String id) {
		this.id = id;
		}

		public String getNombre() {
		return nombre;
		}

		public void setNombre(String nombre) {
		this.nombre = nombre;
		}

		public String getApellidos() {
		return apellidos;
		}

		public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
		}

		public String getEmail() {
		return email;
		}

		public void setEmail(String email) {
		this.email = email;
		}

		public String getDni() {
		return dni;
		}

		public void setDni(String dni) {
		this.dni = dni;
		}

		public String getNacimiento() {
		return nacimiento;
		}

		public void setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
		}

		public String getTelefono() {
		return telefono;
		}

		public void setTelefono(String telefono) {
		this.telefono = telefono;
		}

		public String getEstadoCity() {
		return estadoCity;
		}

		public void setEstadoCity(String estadoCity) {
		this.estadoCity = estadoCity;
		}

		public Object getNumeroCity() {
		return numeroCity;
		}

		public void setNumeroCity(Object numeroCity) {
		this.numeroCity = numeroCity;
		}

		public List<String> getIntereses() {
		return intereses;
		}

		public void setIntereses(List<String> intereses) {
		this.intereses = intereses;
		}

		public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		}

	}
	
	
}
