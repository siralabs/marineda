package com.ladorian.marinedacity;




import com.ladorian.marinedacity.helper.DatabaseHelper;
import com.ladorian.marinedacity.model.Local;
import com.ladorian.marinedacity.model.Operador;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;


public class PlanoFragment extends Fragment implements OnCheckedChangeListener  {
	private Operador mOperador;
	private Local mLocal;
	private boolean isChild;
	
	public PlanoFragment (boolean isChild) {
		this.isChild = isChild;
	}
	

	public void setmOperador(Operador operador) {
		this.mOperador = operador;
	}



	void changeMap(int map) {
		ChildPlanoFragment plano = new ChildPlanoFragment(map);
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		ft.replace(R.id.mapContainer, plano).commit();
	}



	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.map_fragment, null);
		if(mOperador!=null) {
			DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
			mLocal = db.getLocalFromOperador(mOperador);
			db.closeDB();
		}
		RadioGroup rdioG = (RadioGroup)v.findViewById(R.id.segmentedcontrol);
		ChildPlanoFragment plano;
		if (mLocal != null) {
			plano = new ChildPlanoFragment(Integer.parseInt(mLocal.getPlanta()));
			plano.setLocal(mLocal);
			plano.setOperador(mOperador);
			checkBtn(rdioG,Integer.parseInt(mLocal.getPlanta()));
		} else {
			plano = new ChildPlanoFragment(0);
			RadioButton mapzeroBtn = (RadioButton)v.findViewById(R.id.map0);
			mapzeroBtn.setChecked(true);
		}
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		ft.add(R.id.mapContainer, plano).commit();
		rdioG.setOnCheckedChangeListener(this);
		ImageButton backBtn = (ImageButton)v.findViewById(R.id.closebtn);
		if(!isChild) {
			backBtn.setVisibility(View.GONE);
		} else {
			backBtn.setOnClickListener(new OnClickListener () {

				@Override
				public void onClick(View v) {
					((MainActivity)getActivity()).onBackPressed();					
				}
				
			});
		}
		return v;
	}
	
	private void checkBtn(RadioGroup group, int planta) {
		Integer idCheck = new Integer(R.id.map0);
		switch (planta) {
		case 0:
			idCheck = new Integer(R.id.map0);
			break;
		case 1:
			idCheck = new Integer(R.id.map1);
			break;
		case 2:
			idCheck = new Integer(R.id.map2);
			break;
		}
		group.check(idCheck);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.map0:
			changeMap(0);
			break;
		case R.id.map1:
			changeMap(1);
			break;
		case R.id.map2:
			changeMap(2);
			break;
		}
		
	}
	
	
}
