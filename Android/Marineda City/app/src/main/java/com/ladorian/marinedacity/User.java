package com.ladorian.marinedacity;

import java.util.ArrayList;

public class User {
	final String email;
	final String nombre;
	final String apellidos;
	final String telefono;
	final String dni;
	final ArrayList<String> intereses;
	final String nacimiento;
	final String password;
	final String estadoCity;
	
	User(String email, String nombre, String apellidos, String telefono, String dni, ArrayList<String> intereses, String nacimiento, String password,String estadoCity) {
		this.email = email;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.dni = dni;
		this.intereses = intereses;
		this.nacimiento = nacimiento;
		this.password = password;
		this.estadoCity = estadoCity;
	}
}
