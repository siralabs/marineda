package com.ladorian.marinedacity;

import java.util.ArrayList;
import java.util.List;

import com.ladorian.marinedacity.helper.DatabaseHelper;
import com.ladorian.marinedacity.model.Evento;
import com.ladorian.marinedacity.model.Oferta;
import com.ladorian.marinedacity.model.Operador;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MisFavoritosFragment extends Fragment implements OnCheckedChangeListener, OnItemClickListener, OnItemLongClickListener {
	
	private FavoritosListAdapter mAdapter;
	private ArrayList<Object> favoritos;
	private Context mContext;
	
	public MisFavoritosFragment(Context context) {
		mContext = context;
		favoritos = new ArrayList<Object>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = (View)inflater.inflate(R.layout.mis_favoritos_fragment, null);
		RadioGroup rdioG = (RadioGroup)v.findViewById(R.id.segmentedcontrol);
		ListView lv = (ListView)v.findViewById(R.id.favoritosList);
		mAdapter = new FavoritosListAdapter();
		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(this);
		lv.setOnItemLongClickListener(this);
		mAdapter.getShops();
		rdioG.setOnCheckedChangeListener(this);
		return v;	
	}
	
	

	@Override
	public void onDestroy() {
		super.onDestroy();
		mAdapter = null;
	}


	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.map0:
			mAdapter.getShops();
			break;
			
		case R.id.map1:
			mAdapter.getOfertas();
			break;
			
		case R.id.map2:
			mAdapter.getEventos();
			break;
		}
		
	}
	
	private class FavoritosListAdapter extends BaseAdapter {
		

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return favoritos.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return favoritos.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				LayoutInflater inflater = getActivity().getLayoutInflater();
				convertView = inflater.inflate(R.layout.shop_cell, null);
			}
			ImageView icon = (ImageView)convertView.findViewById(R.id.shop_icon);
			TextView nombre = (TextView)convertView.findViewById(R.id.shop_name);
			if(getItem(position) instanceof Operador) {
				Operador item = (Operador)getItem(position);
				nombre.setText(item.getNombre());
				icon.setVisibility(View.VISIBLE);
				if(item.getLogo_bitmap() != null) {
				 icon.setImageBitmap(BitmapFactory.decodeByteArray(item.getLogo_bitmap(), 0, item.getLogo_bitmap().length));
				 
				} else {
					icon.setImageURI(Uri.parse(item.getLogo()));
				}
			} else if (getItem(position) instanceof Oferta) {
				icon.setVisibility(View.INVISIBLE);
				Oferta item = (Oferta)getItem(position);
				nombre.setText(item.getNombre());
			} else if (getItem(position) instanceof Evento) {
				icon.setVisibility(View.INVISIBLE);
				Evento item = (Evento)getItem(position);
				nombre.setText(item.getNombre());
			}
						
			return convertView;
		}
		
		
		public void getShops() {
			DatabaseHelper db= new DatabaseHelper(mContext);
			List<Operador> operadores = db.getOperadorFavorito();
			db.closeDB();
			favoritos.clear();
			favoritos.addAll(operadores);
			notifyDataSetChanged();
		}
		
		public void getOfertas() {
			DatabaseHelper db = new DatabaseHelper(mContext);
			List<Oferta> ofertas = db.getFavoritoOferta();
			db.closeDB();
			favoritos.clear();
			favoritos.addAll(ofertas);
			notifyDataSetChanged();
		}
		
		public void getEventos() {
			DatabaseHelper db = new DatabaseHelper(mContext);
			List<Evento> eventos = db.getFavoritoEvento();
			db.closeDB();
			favoritos.clear();
			favoritos.addAll(eventos);
			notifyDataSetChanged();
		}
		
		public void removeItem(int position) {
			DatabaseHelper db = new DatabaseHelper(mContext);
			if(getItem(position) instanceof Operador) {
				db.removeOperadorFavorito(((Operador)getItem(position)).getId());
			} else if (getItem(position) instanceof Oferta) {
				db.removeOfertaFavorito(((Oferta)getItem(position)).getId());
			} else if (getItem(position) instanceof Evento) {
				db.removeEventoFavorito(((Evento)getItem(position)).getId());
			}
			db.closeDB();
			favoritos.remove(position);
			notifyDataSetChanged();
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if(mAdapter.getItem(position) instanceof Operador) {
			((MainActivity)getActivity()).openDetail((Operador)mAdapter.getItem(position));
		} else if (mAdapter.getItem(position) instanceof Oferta) {
			((MainActivity)getActivity()).openEventDetail((Oferta)mAdapter.getItem(position));
		} else if (mAdapter.getItem(position) instanceof Evento) {
			((MainActivity)getActivity()).openEventDetail((Evento)mAdapter.getItem(position));
		}
		
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
		adb.setTitle("Borrar de favoritos");
		adb.setMessage("¿Seguro que quieres borrarlo de tus favoritos?");
		final int itemToRemove  = position;
		adb.setPositiveButton("Borrar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mAdapter.removeItem(itemToRemove);				
			}
			
		});
		adb.setNegativeButton("Cancelar", null);
		adb.show();
		return false;
	}

}
