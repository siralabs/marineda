package com.ladorian.marinedacity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class ParadasMapFragment extends SupportMapFragment {
	 private SupportMapFragment mSupportMapFragment;
	 private GoogleMap googleMap;
	 private String mNombre;
	 private String mId;
	 private String mPosx;
	 private String mPosy;
	 private String mFrecuencias;
	 private Location myLocation;
	 private FrecuenciasArrayAdapter mAdapter;
	 private final ArrayList <Frecuencia> mListFrecuencias = new ArrayList<Frecuencia> ();
	 
	public Location getMyLocation() {
		return myLocation;
	}

	public void setMyLocation(Location myLocation) {
		this.myLocation = myLocation;
	}

	public void setmNombre(String mNombre) {
		this.mNombre = mNombre;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public void setmPosx(String mPosx) {
		this.mPosx = mPosx;
	}

	public void setmPosy(String mPosy) {
		this.mPosy = mPosy;
	}
	


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		    super.onCreateView(inflater, container, savedInstanceState);
		    View root = inflater.inflate(R.layout.parada_map, null,false);
		    ViewGroup mapHost = (ViewGroup)root.findViewById(R.id.mapHost);
		    mapHost.requestTransparentRegion(mapHost);
		    ListView list = (ListView)root.findViewById(R.id.paradasList);
		    mAdapter = new FrecuenciasArrayAdapter(getActivity(),R.layout.servicios_group_cell,mListFrecuencias);
		    list.setAdapter(mAdapter);
		    TextView title = (TextView)root.findViewById(R.id.contentTitle);
		    title.setText(mNombre);
		    ImageButton back = (ImageButton)root.findViewById(R.id.closebtn);
		    back.setOnClickListener(new View.OnClickListener () {
		    	@Override
		    	public void onClick(View v) {
		    		mSupportMapFragment.getView().animate().alpha(0).setDuration(500).setInterpolator(new AccelerateInterpolator());
		    		((MainActivity)getActivity()).onBackPressed();
		    	}
		    });
		    initializeMap();
		    return root;
	}
	
	
	private class FrecuenciasArrayAdapter extends  ArrayAdapter<Frecuencia> {
		private final Context context;
		private ArrayList <Frecuencia> frec;
		public FrecuenciasArrayAdapter(Context context,int resourceId, ArrayList <Frecuencia> frec ) {
			super(context,R.layout.servicios_group_cell,frec);
			this.context = context;
			this.frec = frec;
		}
		public void updateFrecuenciasList(ArrayList<Frecuencia> newList) {
			this.frec.clear();
			this.frec.addAll(newList);
			this.notifyDataSetChanged();
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
			        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.servicios_group_cell, null, false);
			TextView titleView = (TextView)rowView.findViewById(R.id.titleLabel);
			titleView.setText("Bus nº "+((Frecuencia)frec.get(position)).getmBus()+" a "+((Frecuencia)frec.get(position)).getmDistancia()+"m");
			TextView subtitleView = (TextView)rowView.findViewById(R.id.subtitleLabel);
			subtitleView.setText("Llegada en "+((Frecuencia)frec.get(position)).getmTiempo()+" min");
			ImageView icon = (ImageView)rowView.findViewById(R.id.iconImage);
			icon.setImageResource(R.drawable.trans_bus);
			return rowView;
		}
		
		
	}
	
	private void initializeMap() {
		mSupportMapFragment = (SupportMapFragment)getFragmentManager().findFragmentById(R.id.paradamap);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		//mSupportMapFragment = SupportMapFragment.newInstance();
		mSupportMapFragment = new SupportMapFragment() {
			@Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);
                googleMap = mSupportMapFragment.getMap();
                //PolylineOptions line = new PolylineOptions();
                LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
                if (googleMap != null) {
                	googleMap.setMyLocationEnabled(true);
                	LatLng paradaPos = new LatLng(Double.parseDouble(mPosy.replace(",",".")),Double.parseDouble(mPosx.replace(",",".")));
                	LatLng userPos = new LatLng(myLocation.getLatitude(),myLocation.getLongitude());
                	latLngBuilder.include(userPos);
                	latLngBuilder.include(paradaPos);
                	//googleMap.addPolyline(line);
                	LatLngBounds latLngBounds = latLngBuilder.build();
                	Display display = getActivity().getWindowManager().getDefaultDisplay();
                	Point size = new Point();
                	display.getSize(size);
                	MarkerOptions positionMarkerOptions = new MarkerOptions()
                		.position(paradaPos)
                		.title(mNombre);
                	
                	Marker paradaMarker = googleMap.addMarker(positionMarkerOptions);
                	paradaMarker.showInfoWindow();
                	paradaMarker.setAlpha(0.9f);
                	
                	CameraUpdate movement = CameraUpdateFactory.newLatLngBounds(latLngBounds,size.x,240,40);
                	googleMap.moveCamera(movement);              	   				
                }
			}
		};
		//fragmentTransaction.setCustomAnimations(R.anim.abc_fade_in,R.anim.abc_fade_out);
		fragmentTransaction.replace(R.id.paradamap, mSupportMapFragment).commit();
	}
	

	
	
	@Override
	public void onResume() {
		super.onResume();
		new FrecuenciasTask().execute();
	}



	private class FrecuenciasTask extends AsyncTask<Void,Void,String> {
		@Override
		protected String doInBackground(Void... params) {
			String address = "http://itranvias.com/queryitr.php?&func=0&dato="+mId;
			StringBuilder builder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
	 		HttpGet httpGet = new HttpGet(address);
			try{
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if(statusCode == 200){
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while((line = reader.readLine()) != null){
						builder.append(line);
					}
				} else {
					Log.e(MainActivity.class.toString(),"Failedet JSON object");
				}
			}catch(ClientProtocolException e){
				e.printStackTrace();
			} catch (IOException e){
				e.printStackTrace();
			}
			return builder.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			mFrecuencias = result;
			addFrecuencias();
		}

	}
	
	public void addFrecuencias() {
		if(mFrecuencias !=null) {
			 try{
		        	JSONObject jsonObject = new JSONObject(mFrecuencias);
		        	JSONObject busesArray = jsonObject.getJSONObject("buses");
		        	JSONArray lineasArray = busesArray.getJSONArray("lineas");
		        	final ArrayList <Frecuencia> newArray = new ArrayList<Frecuencia> ();
		        	for( int i = 0; i < lineasArray.length();i++) {
		        		if(lineasArray.getJSONObject(i).getString("nom_comer").equals("11")) {
		        			JSONArray frecuenciasArray = lineasArray.getJSONObject(i).getJSONArray("buses");
		        			for (int j = 0; j < frecuenciasArray.length();j++) {
		        				Frecuencia frec = new Frecuencia();
		        				frec.setmBus(frecuenciasArray.getJSONObject(j).getString("bus"));
		        				frec.setmDistancia(frecuenciasArray.getJSONObject(j).getString("distancia"));
		        				frec.setmTiempo(frecuenciasArray.getJSONObject(j).getString("tiempo"));
		        				newArray.add(frec);
		        			}
		        		}
		        	}
		        	Collections.sort(newArray, new Comparator<Object> () {
						@Override
						public int compare(Object lhs, Object rhs) {
							final Integer i1 = Integer.parseInt(((Frecuencia)lhs).getmTiempo());
							final Integer i2 = Integer.parseInt(((Frecuencia)rhs).getmTiempo());
							return new Integer(i1).compareTo(new Integer(i2));
						}		        		
		        	});
		        	mAdapter.updateFrecuenciasList(newArray);
		        } catch(Exception e){e.printStackTrace();}
		        finally{
		        	Log.d("parseado",Integer.toString(mListFrecuencias.size()));
		        	 
		        	}
		}
	}
	
	public class Frecuencia {
		private String mTiempo;
		private String mDistancia;
		private String mBus;
		public String getmTiempo() {
			return mTiempo;
		}
		public void setmTiempo(String mTiempo) {
			this.mTiempo = mTiempo;
		}
		public String getmDistancia() {
			return mDistancia;
		}
		public void setmDistancia(String mDistancia) {
			this.mDistancia = mDistancia;
		}
		public String getmBus() {
			return mBus;
		}
		public void setmBus(String mBus) {
			this.mBus = mBus;
		}
		
	}
	
	
}
