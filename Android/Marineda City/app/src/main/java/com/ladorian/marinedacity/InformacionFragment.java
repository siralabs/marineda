package com.ladorian.marinedacity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class InformacionFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.informacion, null);
		TextView telefono = (TextView)v.findViewById(R.id.telefono);
		telefono.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				String phoneNumber = "881888888";
				callIntent.setData(Uri.parse("tel:"+phoneNumber));
				startActivity(callIntent);
			}
			
		});
		TextView link = (TextView)v.findViewById(R.id.link);
		link.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String url="http://www.marinedacity.com";
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				try {
					startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		TextView descripcion = (TextView)v.findViewById(R.id.descripcion);
		descripcion.setText("Marineda City es la gran ciudad de Ocio&Shopping del noroeste de España. Un polo comercial, de entretenimiento, turístico y de negocios en el que las mejores marcas locales, nacionales e internacionales ya han apostado. Moda, hogar, electrónica, complementos, ocio, óptica, bricolaje, deportes, gastronomía… Productos y servicios que ya son todo un referente para los 15 millones de personas que nos visitan anualmente.   ");
		TextView clockText = (TextView)v.findViewById(R.id.clockText);
//		clockText.setText(Html.fromHtml("<![CDATA[]]>"));
		Spanned clockValue = Html.fromHtml("<b>Shopping:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de 10 a 22h.<br>Hipercor / Decathlon / Feuvert:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de 9 a 22 h<br><br><b>Ocio y restauración:</b><br> De domingo a jueves:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de 12 a 00h.<br>Viernes, sábados y vísperas de apertura:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de 12 a 01h.<br><br>DÍAS ESPECIALES DE APERTURA: <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5 julio<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11 octubre<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 noviembre<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6, 12, 20, 27 diciembre");
		clockText.setText(clockValue);

		return v;
	}

}
