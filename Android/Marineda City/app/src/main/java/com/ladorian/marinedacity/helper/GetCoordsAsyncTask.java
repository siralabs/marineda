package com.ladorian.marinedacity.helper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.ladorian.marinedacity.ApiClient;
import com.ladorian.marinedacity.Constants;
import com.ladorian.marinedacity.model.Local;
import com.ladorian.marinedacity.model.Operador;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

public class GetCoordsAsyncTask extends AsyncTask<String, String, Void> {
	private Context mContext;
	private Activity mActivity;
	DatabaseHelper db;
	
	public GetCoordsAsyncTask (Context context, Activity activity) {
		mContext = context;
		mActivity = activity;
		db = new DatabaseHelper(mContext);
	}


	@Override
	protected Void doInBackground(String... params) {
		JSONArray json = null;
        String str = "";
        HttpResponse response;
        HttpClient myClient = new DefaultHttpClient();
        HttpPost myConnection = new HttpPost(Constants.COORDENADAS_URL);
         
        try {
            response = myClient.execute(myConnection);
            str = EntityUtils.toString(response.getEntity(), "UTF-8");
             
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
                 
        try{
            JSONArray jArray = new JSONArray(str);
            for (int i=0;i<jArray.length();i++) {
            	JSONArray locales = jArray.getJSONArray(i);
            	for (int j=0;j<locales.length();j++) {
            		JSONObject localData = locales.getJSONObject(j);
            		Local local = new Local();
            		local.setNombre(localData.getString("nombre"));
            		local.setLatitud(localData.getString("latitud"));
            		local.setLongitud(localData.getString("longitud"));
            		local.setPlanta(Integer.toString(i));
            		db.createLocal(local);
            	}
            }
            
                        
        } catch ( JSONException e) {
            e.printStackTrace();                
        }
		return null;              
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		//Log.d("LOCALES EN BBDD:",db.getAllLocales().toString());
		DownloadShopsTask sdt = new DownloadShopsTask();
		sdt.execute();
	}
	
	private class DownloadShopsTask extends AsyncTask <Void,Void,Void> {

		@Override
		protected Void doInBackground(Void... params) {
			ApiClient.getMarinedaCityApiClient().getOperadores(new Callback <List<Operador>> () {

				@Override
				public void failure(RetrofitError arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void success(List<Operador> operadores, Response arg1) {
					int operadoresInList = operadores.size();
					for (Operador operador : operadores) {
						try {
							db.createOperador(operador);
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					Log.d("OPERADORES EN BBDD:",db.getAllOperadores().toString());
					SharedPreferences prefs = mActivity.getSharedPreferences("firstRun", Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = prefs.edit();
					editor.putBoolean("isFirst", false);
					editor.putInt("totalOperadores", operadoresInList);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String currentDate = sdf.format(new Date());
					editor.putString("updated", currentDate);
					editor.commit();
					db.closeDB(); // closing database
					mActivity.finish();
				}
				
			});
			return null;
		}
				
	}
		

}
