package com.ladorian.marinedacity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class WaitingDialog extends DialogFragment {
	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		ProgressDialog dialog = new ProgressDialog(getActivity());
		this.setStyle(STYLE_NO_TITLE, getTheme());
		dialog.setMessage("Espere...");
		setCancelable(false);
		return dialog;
	}

}
