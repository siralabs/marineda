package com.ladorian.marinedacity;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ParkingRecoveryFragment extends Fragment {
	
	private Context mContext;
	private static final String JSON_PLANTA = "numPlanta";
	private static final String JSON_PLAZA = "numPlaza";
	private static final String JSON_COLOR = "color";
	private static final String JSON_LETRA = "letra";
	private static final String JSON_FECHA = "fecha";
	private static final String FILENAME = "parking.json";
	private JSONObject json;
	private String mColor;
	private String mPlaza;
	private String mPlanta;
	private String mFecha;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		try {
			this.recoverData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void recoverData() throws IOException, JSONException {
		BufferedReader reader = null;
		try {
			InputStream in = mContext.openFileInput(FILENAME);
			reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder jsonString = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null) {
				jsonString.append(line);
			}
			json = (JSONObject) new JSONTokener(jsonString.toString()).nextValue();
			mColor = json.getString(JSON_COLOR);
			mPlaza = json.getString(JSON_LETRA)+" "+json.getString(JSON_PLAZA);
			mPlanta = "P. "+json.getString(JSON_PLANTA);
			mFecha = "Guardado: "+json.getString(JSON_FECHA);
		} catch (FileNotFoundException e) {
			
		}finally {
			if(reader !=null) {
				reader.close();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.parking_recovery, null);
		View colorPlaza = (View)v.findViewById(R.id.colorPlaza);
		GradientDrawable circulo = (GradientDrawable)colorPlaza.getBackground();
		if(mColor != null) {
			if(mColor.equals("red_button")) {
				circulo.setColor(getResources().getColor(R.color.red_button));
			} else if(mColor.equals("green_button")) {
				circulo.setColor(getResources().getColor(R.color.green_button));
			} else if(mColor.equals("malva_button")) { 
				circulo.setColor(getResources().getColor(R.color.malva_button));
			} else if(mColor.equals("blue_button")) { 
				circulo.setColor(getResources().getColor(R.color.blue_button));
			} else if(mColor.equals("orange_button")) { 
				circulo.setColor(getResources().getColor(R.color.orange_button));
			}
		}
		TextView numPlaza = (TextView)v.findViewById(R.id.numPlaza);
		TextView numPlanta = (TextView)v.findViewById(R.id.numPlanta);
		TextView fecha = (TextView)v.findViewById(R.id.fechaGuardado);
		if(mPlaza != null) {
			numPlaza.setText(mPlaza);
			numPlaza.setTextColor(getResources().getColor(android.R.color.white));
		}
		if(mPlanta != null) {
			numPlanta.setText(mPlanta);
			numPlanta.setVisibility(View.VISIBLE);
		}
		if(mFecha != null) fecha.setText(mFecha);
		//circulo.setColor(getResources().getColor(R.color.red_button));
		return v;
	}
	
}
