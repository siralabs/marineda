package com.ladorian.marinedacity.helper;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.ladorian.marinedacity.ApiClient;
import com.ladorian.marinedacity.R;
import com.ladorian.marinedacity.model.Oferta;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class OfertaListAdapter extends BaseAdapter {

    private Activity activity;
    private List<Oferta> data;
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;
    ProgressBar bar;

    public OfertaListAdapter(Activity a, ProgressBar pb) {
        activity = a;
        bar = pb;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
        data = new ArrayList<Oferta>();
        ApiClient.getMarinedaCityApiClient().getOfertas(new Callback<List<Oferta>>() {
            @Override
            public void success(List<Oferta> ofertas, Response response) {
                Log.d("Ofertas", ofertas.get(0).getNombre());

                addOfertas(ofertas);
                //adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                if (retrofitError == null) {
                    Log.d("Api", "Error");
                } else {
                    Log.d("Api", "Existe error");
                }
            }
        });
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        Oferta oferta = data.get(position);
        return oferta;
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.event_cell, null);
        }
        Oferta oferta = (Oferta) this.getItem(position);
        TextView nombre = (TextView) convertView.findViewById(R.id.event_name);
        nombre.setText(oferta.getNombre());
        ImageView image = (ImageView) convertView.findViewById(R.id.event_thumbnail);
        imageLoader.DisplayImage(oferta.getMiniatura(), image);

        return convertView;
    }

    public void addOfertas(List<Oferta> ofertas) {
        this.data.clear();
        this.data.addAll(ofertas);
        if(ofertas.size()==0){
            showNoDataMessage();
        }

        activity.runOnUiThread(new Runnable() {
            public void run() {
                bar.setVisibility(View.GONE);
                notifyDataSetChanged();
            }
        });

    }
    public void showNoDataMessage() {
        String msg ="No hay ofertas disponibles.";
        new android.app.AlertDialog.Builder(this.activity)
                .setTitle("Ofertas")
                .setMessage(msg)
                .setPositiveButton("Cerrar",null)
                .show();
    }
}
