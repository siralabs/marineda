package com.ladorian.marinedacity.helper;

import java.io.IOException;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.ladorian.marinedacity.model.Evento;
import com.ladorian.marinedacity.model.Local;
import com.ladorian.marinedacity.model.Oferta;
import com.ladorian.marinedacity.model.Operador;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private Context mContext;
	
	private static final String LOG = "DatabaseHelper";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "marinedaCity";
	
	//Table names
	private static final String TABLE_OPERADOR = "operadores";
	private static final String TABLE_OFERTA = "ofertas";
	private static final String TABLE_LOCAL = "locales";
	private static final String TABLE_EVENTO = "eventos";
	
	//Common column names 
	private static final String KEY_ID = "id";
	private static final String KEY_NOMBRE = "nombre";
	private static final String KEY_OPERADOR = "operador";
	private static final String KEY_INICIO = "inicio";
	private static final String KEY_FIN = "fin";
	private static final String KEY_DESCRIPCION = "descripcion";
	private static final String KEY_IMAGEN = "imagen";
	private static final String KEY_DETALLE = "detalle";
	private static final String KEY_LOGO_BITMAP = "logo_bmp";
	
	//OPERADOR Table - column names
	private static final String KEY_CATEGORIA = "categoria";
	private static final String KEY_EMAIL = "email";
	private static final String KEY_FAVORITO = "favorito";
	private static final String KEY_LINK = "link";
	private static final String KEY_LOCAL = "local";
	private static final String KEY_LOGO = "logo";
	private static final String KEY_TELEFONO = "telefono";
	private static final String KEY_BASE_ID = "base_id";
	
	//LOCAL Table - column names 
	private static final String KEY_PLANTA = "planta";
	private static final String KEY_ID_OPERADOR = "id_operador";
	private static final String KEY_LATITUD ="latitud";
	private static final String KEY_LONGITUD = "longitud";
	
	//Table create statements
	//OPERADOR table create statement
	private static final String CREATE_TABLE_OPERADOR = "CREATE TABLE "+ TABLE_OPERADOR
			+ " ( " +KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +KEY_BASE_ID+" INTEGER NOT NULL, " + KEY_NOMBRE + " TEXT NOT NULL, " + KEY_CATEGORIA + " VARCHAR(64) NOT NULL, " + KEY_EMAIL +" TEXT, "
			+ KEY_TELEFONO + " VARCHAR(16), " + KEY_IMAGEN + " TEXT NOT NULL, "+ KEY_LOGO + " TEXT NOT NULL, " + KEY_LOGO_BITMAP + " BLOB, " + KEY_LINK + " TEXT, " + KEY_LOCAL + " TEXT, " + KEY_DESCRIPCION + " TEXT, "
			+ KEY_FAVORITO + " TINYINT(1) DEFAULT 0 " + " );";
	
	//OFERTA table create statement
	private static final String CREATE_TABLE_OFERTA = "CREATE TABLE " + TABLE_OFERTA
			+ " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NOMBRE + " TEXT NOT NULL, " + KEY_IMAGEN + " TEXT NOT NULL, " + KEY_DESCRIPCION + " TEXT, " + KEY_DETALLE + " TEXT, "
			+ KEY_INICIO + " TEXT, " + KEY_FIN + " TEXT, " + KEY_OPERADOR + " TEXT, " + KEY_ID_OPERADOR + " INTEGER, " + "FOREIGN KEY (" + KEY_ID_OPERADOR + ") REFERENCES " + TABLE_OPERADOR + " ("
			+ KEY_ID + ")ON DELETE CASCADE) ;";
	
	//OFERTA table create statement
		private static final String CREATE_TABLE_EVENTO = "CREATE TABLE " + TABLE_EVENTO
				+ " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NOMBRE + " TEXT NOT NULL, " + KEY_IMAGEN + " TEXT NOT NULL, " + KEY_DESCRIPCION + " TEXT, " + KEY_DETALLE + " TEXT, "
				+ KEY_INICIO + " TEXT, " + KEY_FIN + " TEXT, " + KEY_OPERADOR + " TEXT, " + KEY_ID_OPERADOR + " INTEGER, " + "FOREIGN KEY (" + KEY_ID_OPERADOR + ") REFERENCES " + TABLE_OPERADOR + " ("
				+ KEY_ID + ")ON DELETE CASCADE) ;";
	
	//LOCAL table create statement
	private static final String CREATE_TABLE_LOCAL = "CREATE TABLE " + TABLE_LOCAL
			+ " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NOMBRE + " TEXT NOT NULL, " + KEY_PLANTA + " TEXT NOT NULL, " + KEY_LATITUD + " TEXT NOT NULL, " + KEY_LONGITUD + " TEXT NOT NULL, " + KEY_ID_OPERADOR + " INTEGER, " + "FOREIGN KEY ("
			+ KEY_ID_OPERADOR + ") REFERENCES " + TABLE_OPERADOR + " (" + KEY_ID + ") ON DELETE SET NULL);";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_OPERADOR);
		db.execSQL(CREATE_TABLE_OFERTA);
		db.execSQL(CREATE_TABLE_LOCAL);
		db.execSQL(CREATE_TABLE_EVENTO);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_LOCAL);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_OFERTA);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_OPERADOR);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_EVENTO);
		
		onCreate(db);

	}
	
	//Creating an OPERADOR
	public long createOperador(Operador operador) throws ClientProtocolException, IOException {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NOMBRE, operador.getNombre());
		values.put(KEY_CATEGORIA, operador.getCategoria());
		values.put(KEY_EMAIL, operador.getEmail());
		values.put(KEY_TELEFONO, operador.getTelefono());
		values.put(KEY_IMAGEN, operador.getImagen());
		values.put(KEY_LOGO, operador.getLogo());
		values.put(KEY_LINK, operador.getLink());
		values.put(KEY_LOCAL, operador.getLocal());
		values.put(KEY_DESCRIPCION, operador.getDescripcion());
		values.put(KEY_BASE_ID, operador.getId());
		long operador_id = db.insert(TABLE_OPERADOR, null, values);
		
		for (Local local : operador.getLocales()) {
			asignarLocal(local,operador_id);
		}
		
		SaveLogoAsyncTask sl = new SaveLogoAsyncTask();
		sl.execute(new String [] {operador.getLogo(),Integer.toString(operador.getId())});
		return operador_id;
	}
	
	public int asignarLocal (Local local, long operador_id) {
		SQLiteDatabase db = this.getWritableDatabase();		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_ID_OPERADOR, operador_id);
	    return db.update(TABLE_LOCAL, values, KEY_NOMBRE + " = ? AND " + KEY_PLANTA + " = ?", new String[] {local.getNombre(),local.getPlanta()});
	}
	
	public int reasignarLocal (Local local, int operador_id) {
		SQLiteDatabase db = this.getWritableDatabase();		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_ID_OPERADOR, "");
	    db.update(TABLE_LOCAL, values, KEY_ID_OPERADOR + " = ? ", new String[] {Integer.toString(operador_id)});
	    ContentValues newValues = new ContentValues();
	    newValues.put(KEY_ID_OPERADOR, operador_id);
	    return db.update(TABLE_LOCAL, newValues, KEY_NOMBRE + " = ? AND " + KEY_PLANTA + " = ?", new String[] {local.getNombre(),local.getPlanta()});
	}
	
	private class SaveLogoAsyncTask extends AsyncTask <String,Void,Void> {

			@Override
			protected Void doInBackground(String... params) {
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
				HttpGet mHttpGet = new HttpGet(params[0]);
				HttpResponse mHttpResponse;
				try {
					mHttpResponse = mHttpClient.execute(mHttpGet);
					if(mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						HttpEntity entity = mHttpResponse.getEntity();
						if(entity != null) {
							//values.put(KEY_LOGO_BITMAP, EntityUtils.toByteArray(entity));
							saveOperadorLogo(params[1],EntityUtils.toByteArray(entity));
						}
					}
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
				return null;
			}
			
		}
	
	public int saveOperadorLogo(String id,byte[] bitmap) {
		SQLiteDatabase db = this.getWritableDatabase();		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_LOGO_BITMAP, bitmap);	    
		return db.update(TABLE_OPERADOR, values, KEY_BASE_ID + " = ?", new String[] {id});
	}
	
	public long countOperadores() {
		SQLiteDatabase db = this.getReadableDatabase();
		 return DatabaseUtils.queryNumEntries(db, TABLE_OPERADOR);
	}
	
	public int isSaved(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor dataCount = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_OPERADOR + " WHERE "+ KEY_BASE_ID + " = ?", new String [] {Integer.toString(id)});
		dataCount.moveToFirst();
		if(dataCount.getCount() > 0 && dataCount.getColumnCount() > 0) {
			return dataCount.getInt(0);
		} else {
			return 0;
		}
	}
	
	public int operadorFavorito (long id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_FAVORITO, 1);
		return db.update(TABLE_OPERADOR, values, KEY_ID + " = ? ", new String[] {Long.toString(id)}	);
	}
	
	public void removeOperadorFavorito (int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_FAVORITO, 0);
		int updated = db.update(TABLE_OPERADOR,values,KEY_ID+" = ? ",new String[] {Integer.toString(id)});
		if (updated > 0) {
			Toast toast = Toast.makeText(mContext, "Favorito eliminado", Toast.LENGTH_LONG);
			toast.show();
		}
	}
	
	public List<Operador> getOperadorFavorito() {
		List<Operador> operadores = new ArrayList<Operador>();
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_OPERADOR + " WHERE " + KEY_FAVORITO + " = 1 ORDER BY "+KEY_NOMBRE+" ASC";
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()) {
			do {
				Operador op = new Operador();
				op.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				op.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
				op.setCategoria(c.getString(c.getColumnIndex(KEY_CATEGORIA)));
				op.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
				op.setTelefono(c.getString(c.getColumnIndex(KEY_TELEFONO)));
				op.setImagen(c.getString(c.getColumnIndex(KEY_IMAGEN)));
				op.setLogo(c.getString(c.getColumnIndex(KEY_LOGO)));
				op.setLink(c.getString(c.getColumnIndex(KEY_LINK)));
				op.setLocal(c.getString(c.getColumnIndex(KEY_LOCAL)));
				op.setDescripcion(c.getString(c.getColumnIndex(KEY_DESCRIPCION)));
				byte [] bb = c.getBlob(c.getColumnIndex(KEY_LOGO_BITMAP));
				op.setLogo_bitmap(bb);
				op.setBase_id(c.getInt(c.getColumnIndex(KEY_BASE_ID)));
				op.setFavorito(c.getInt(c.getColumnIndex(KEY_FAVORITO)));
				operadores.add(op);
			} while (c.moveToNext());
		}
		return operadores;
	}
	
	//Fetching an OPERADOR
	public Operador getOperador(long operador_id) {
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_OPERADOR + " WHERE " + KEY_ID + " = " + Long.toString(operador_id);
		Log.d(LOG, selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		if(c != null) c.moveToFirst();
		
		Operador op = new Operador();
		op.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		op.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
		op.setCategoria(c.getString(c.getColumnIndex(KEY_CATEGORIA)));
		op.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
		op.setTelefono(c.getString(c.getColumnIndex(KEY_TELEFONO)));
		op.setImagen(c.getString(c.getColumnIndex(KEY_IMAGEN)));
		op.setLogo(c.getString(c.getColumnIndex(KEY_LOGO)));
		op.setLink(c.getString(c.getColumnIndex(KEY_LINK)));
		op.setLocal(c.getString(c.getColumnIndex(KEY_LOCAL)));
		op.setDescripcion(c.getString(c.getColumnIndex(KEY_DESCRIPCION)));
		op.setFavorito(c.getInt(c.getColumnIndex(KEY_FAVORITO)));
		return op;		
	}
	
	public Operador getOperadorBase(int operador_id) {
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_OPERADOR + " WHERE " + KEY_BASE_ID + " = " + Integer.toString(operador_id);
		Log.d(LOG, selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		if(c != null) c.moveToFirst();
		
		Operador op = new Operador();
		op.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		op.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
		op.setCategoria(c.getString(c.getColumnIndex(KEY_CATEGORIA)));
		op.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
		op.setTelefono(c.getString(c.getColumnIndex(KEY_TELEFONO)));
		op.setImagen(c.getString(c.getColumnIndex(KEY_IMAGEN)));
		op.setLogo(c.getString(c.getColumnIndex(KEY_LOGO)));
		op.setLink(c.getString(c.getColumnIndex(KEY_LINK)));
		op.setLocal(c.getString(c.getColumnIndex(KEY_LOCAL)));
		op.setDescripcion(c.getString(c.getColumnIndex(KEY_DESCRIPCION)));
		op.setFavorito(c.getInt(c.getColumnIndex(KEY_FAVORITO)));
		return op;		
	}
	
	public int getOperadorId(String baseId) {
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT id FROM " + TABLE_OPERADOR +" WHERE " + KEY_BASE_ID + " = " + baseId;
		Cursor c = db.rawQuery(selectQuery, null);
		if(c != null) c.moveToFirst();
		
		return c.getInt(c.getColumnIndex(KEY_ID));
	}
	
	public int updateOperador(Operador operador) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NOMBRE, operador.getNombre());
		values.put(KEY_CATEGORIA, operador.getCategoria());
		values.put(KEY_EMAIL, operador.getEmail());
		values.put(KEY_TELEFONO, operador.getTelefono());
		values.put(KEY_IMAGEN, operador.getImagen());
		values.put(KEY_LOGO, operador.getLogo());
		values.put(KEY_LINK, operador.getLink());
		values.put(KEY_LOCAL, operador.getLocal());
		values.put(KEY_DESCRIPCION, operador.getDescripcion());
		values.put(KEY_BASE_ID, operador.getId());
		
		for (Local local : operador.getLocales()) {
			reasignarLocal(local,this.getOperadorId(Integer.toString(operador.getId())));
		}
		
		return db.update(TABLE_OPERADOR, values, KEY_BASE_ID + " = ?", new String[] {Integer.toString(operador.getId())});
	}
	
	public void deleteOperador (int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_OPERADOR, KEY_BASE_ID + " = ? ", new String[] {Integer.toString(id)});
	}
	
	//Fetching all OPERADORES
	public List<Operador> getAllOperadores() {
		List<Operador> operadores = new ArrayList<Operador> ();
		String selectQuery = "SELECT * FROM " + TABLE_OPERADOR + " ORDER BY "+ KEY_NOMBRE + " ASC ";
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToFirst()) {
			do {
				Operador op = new Operador();
				op.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				op.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
				op.setCategoria(c.getString(c.getColumnIndex(KEY_CATEGORIA)));
				op.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
				op.setTelefono(c.getString(c.getColumnIndex(KEY_TELEFONO)));
				op.setImagen(c.getString(c.getColumnIndex(KEY_IMAGEN)));
				op.setLogo(c.getString(c.getColumnIndex(KEY_LOGO)));
				op.setLink(c.getString(c.getColumnIndex(KEY_LINK)));
				op.setLocal(c.getString(c.getColumnIndex(KEY_LOCAL)));
				op.setDescripcion(c.getString(c.getColumnIndex(KEY_DESCRIPCION)));
				byte [] bb = c.getBlob(c.getColumnIndex(KEY_LOGO_BITMAP));
				op.setLogo_bitmap(bb);
				op.setBase_id(c.getInt(c.getColumnIndex(KEY_BASE_ID)));
				op.setFavorito(c.getInt(c.getColumnIndex(KEY_FAVORITO)));
				operadores.add(op);
			} while (c.moveToNext());
		}
		return operadores;
	}
	
	//Fetching OPERADORES by CATEGORIA
	public List<Operador> getCategoryOperadores(String categoria) {
		String selectQuery;
		List<Operador> operadores = new ArrayList<Operador> ();
		if(categoria.equals("OCIO Y RESTAURACIÓN")) {
			selectQuery = "SELECT * FROM " + TABLE_OPERADOR + " WHERE " + KEY_CATEGORIA + " = 'OCIO' OR "+KEY_CATEGORIA+" = 'RESTAURACIÓN' ORDER BY " +KEY_NOMBRE+" ASC;";
		} else {
			selectQuery = "SELECT * FROM " + TABLE_OPERADOR + " WHERE " + KEY_CATEGORIA + " = '" + categoria + "' ORDER BY " +KEY_NOMBRE+" ASC;";
		}
		
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToFirst()) {
			do {
				Operador op = new Operador();
				op.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				op.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
				op.setCategoria(c.getString(c.getColumnIndex(KEY_CATEGORIA)));
				op.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
				op.setTelefono(c.getString(c.getColumnIndex(KEY_TELEFONO)));
				op.setImagen(c.getString(c.getColumnIndex(KEY_IMAGEN)));
				op.setLogo(c.getString(c.getColumnIndex(KEY_LOGO)));
				op.setLink(c.getString(c.getColumnIndex(KEY_LINK)));
				op.setLocal(c.getString(c.getColumnIndex(KEY_LOCAL)));
				op.setDescripcion(c.getString(c.getColumnIndex(KEY_DESCRIPCION)));
				byte [] bb = c.getBlob(c.getColumnIndex(KEY_LOGO_BITMAP));
				op.setLogo_bitmap(bb);
				op.setBase_id(c.getInt(c.getColumnIndex(KEY_BASE_ID)));
				op.setFavorito(c.getInt(c.getColumnIndex(KEY_FAVORITO)));
				operadores.add(op);
			} while (c.moveToNext());
		}
		return operadores;
	}
	
	//Creating OFERTA
	 public boolean createOferta (Oferta oferta) {
		 SQLiteDatabase db = this.getWritableDatabase();
		 Cursor dataCount = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_OFERTA + " WHERE "+ KEY_NOMBRE + " = ? AND " + KEY_OPERADOR +" = ?", new String [] {oferta.getNombre(),oferta.getOperador()});
			dataCount.moveToFirst();
			if(dataCount.getCount() > 0 && dataCount.getColumnCount() > 0) {
				if (dataCount.getInt(0) > 0) {
					String msg = "¡Esta oferta ya se encuentra entre sus favoritas!";
					new AlertDialog.Builder(mContext)
				    .setTitle("Marineda City")
				    .setMessage(msg)
				    .setPositiveButton("Aceptar", null)
				    .show();
					return false;
				} else {
					 ContentValues values = new ContentValues();
					 values.put(KEY_NOMBRE, oferta.getNombre());
					 values.put(KEY_IMAGEN, oferta.getImagen());
					 values.put(KEY_INICIO,oferta.getInicio());
					 values.put(KEY_FIN, oferta.getFin());
					 values.put(KEY_DETALLE, oferta.getDetalle());
					 values.put(KEY_DESCRIPCION, oferta.getDescripcion());
					 values.put(KEY_ID_OPERADOR, Integer.parseInt(oferta.getIdoperador()));
					 values.put(KEY_OPERADOR, oferta.getOperador());
					 
					 long oferta_id = db.insert(TABLE_OFERTA, null, values);
					 if (oferta_id >= 0) {
						 String msg = "¡"+oferta.getNombre()+" se ha guardado en sus favoritos!";
							new AlertDialog.Builder(mContext)
						    .setTitle("Marineda City")
						    .setMessage(msg)
						    .setPositiveButton("Aceptar", null)
						    .show();
						 return true;
					 }					
				}
			}
			String msg = "Ha ocurrido un error al intentar guardar esta oferta. Lo sentimos.";
			new AlertDialog.Builder(mContext)
		    .setTitle("Marineda City")
		    .setMessage(msg)
		    .setPositiveButton("Aceptar", null)
		    .show();
		 return false;
	 }
	 
	 public List<Oferta> getFavoritoOferta() {
		 List<Oferta> ofertas = new ArrayList<Oferta> ();
		 String selectQuery = "SELECT * FROM " + TABLE_OFERTA;
		 SQLiteDatabase db = this.getReadableDatabase();
		 Cursor c = db.rawQuery(selectQuery, null);
		 if(c.moveToFirst()) {
			 do {
				 Oferta of = new Oferta();
				 of.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				 of.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
				 of.setImagen(c.getString(c.getColumnIndex(KEY_IMAGEN)));
				 of.setInicio(c.getString(c.getColumnIndex(KEY_INICIO)));
				 of.setFin(c.getString(c.getColumnIndex(KEY_FIN)));
				 of.setDetalle(c.getString(c.getColumnIndex(KEY_DETALLE)));
				 of.setDescripcion(c.getString(c.getColumnIndex(KEY_DESCRIPCION)));
				 of.setIdoperador(Integer.toString(c.getInt(c.getColumnIndex(KEY_ID_OPERADOR))));
				 of.setOperador(c.getString(c.getColumnIndex(KEY_OPERADOR)));
				 ofertas.add(of);
			 } while(c.moveToNext());
		 }
		 return ofertas;
	 }
	 
	 public void removeOfertaFavorito(int id) {
		 SQLiteDatabase db = this.getWritableDatabase();
		 int removed = db.delete(TABLE_OFERTA, KEY_ID + " = ?", new String[] {Integer.toString(id)});
		 if (removed > 0) {
			Toast toast = Toast.makeText(mContext, "Favorito eliminado", Toast.LENGTH_LONG);
			toast.show();
		 }
	 }
	 
	 
	//Creating EVENTO
		 public boolean createEvento (Evento evento) {
			 SQLiteDatabase db = this.getWritableDatabase();
			 Cursor dataCount = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_EVENTO + " WHERE "+ KEY_NOMBRE + " = ? AND " + KEY_OPERADOR +" = ?", new String [] {evento.getNombre(),evento.getOperador()});
				dataCount.moveToFirst();
				if(dataCount.getCount() > 0 && dataCount.getColumnCount() > 0) {
					if (dataCount.getInt(0) > 0) {
						String msg = "¡Este evento ya se encuentra entre sus favoritos!";
						new AlertDialog.Builder(mContext)
					    .setTitle("Marineda City")
					    .setMessage(msg)
					    .setPositiveButton("Aceptar", null)
					    .show();
						return false;
					} else {
						 ContentValues values = new ContentValues();
						 values.put(KEY_NOMBRE, evento.getNombre());
						 values.put(KEY_IMAGEN, evento.getImagen());
						 values.put(KEY_INICIO,evento.getInicio());
						 values.put(KEY_FIN, evento.getFin());
						 values.put(KEY_DETALLE, evento.getDetalle());
						 values.put(KEY_DESCRIPCION, evento.getDescripcion());
						 values.put(KEY_ID_OPERADOR, Integer.parseInt(evento.getIdoperador()));
						 values.put(KEY_OPERADOR, evento.getOperador());
						 
						 long evento_id = db.insert(TABLE_EVENTO, null, values);
						 if (evento_id >= 0) {
							 String msg = "¡"+evento.getNombre()+" se ha guardado en sus favoritos!";
								new AlertDialog.Builder(mContext)
							    .setTitle("Marineda City")
							    .setMessage(msg)
							    .setPositiveButton("Aceptar", null)
							    .show();
							 return true;
						 }					
					}
				}
				String msg = "Ha ocurrido un error al intentar guardar este evento. Lo sentimos.";
				new AlertDialog.Builder(mContext)
			    .setTitle("Marineda City")
			    .setMessage(msg)
			    .setPositiveButton("Aceptar", null)
			    .show();
			 return false;
		 }
		 
		 public List<Evento> getFavoritoEvento() {
			 List<Evento> eventos = new ArrayList<Evento> ();
			 String selectQuery = "SELECT * FROM " + TABLE_EVENTO;
			 SQLiteDatabase db = this.getReadableDatabase();
			 Cursor c = db.rawQuery(selectQuery, null);
			 if(c.moveToFirst()) {
				 do {
					 Evento of = new Evento();
					 of.setId(c.getInt(c.getColumnIndex(KEY_ID)));
					 of.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
					 of.setImagen(c.getString(c.getColumnIndex(KEY_IMAGEN)));
					 of.setInicio(c.getString(c.getColumnIndex(KEY_INICIO)));
					 of.setFin(c.getString(c.getColumnIndex(KEY_FIN)));
					 of.setDetalle(c.getString(c.getColumnIndex(KEY_DETALLE)));
					 of.setDescripcion(c.getString(c.getColumnIndex(KEY_DESCRIPCION)));
					 of.setIdoperador(Integer.toString(c.getInt(c.getColumnIndex(KEY_ID_OPERADOR))));
					 of.setOperador(c.getString(c.getColumnIndex(KEY_OPERADOR)));
					 eventos.add(of);
				 } while(c.moveToNext());
			 }
			 return eventos;
		 }
		 
		 public void removeEventoFavorito(int id) {
			 SQLiteDatabase db = this.getWritableDatabase();
			 int removed = db.delete(TABLE_EVENTO, KEY_ID + " = ?", new String[] {Integer.toString(id)});
			 if (removed > 0) {
				Toast toast = Toast.makeText(mContext, "Favorito eliminado", Toast.LENGTH_LONG);
				toast.show();
			 }
		 }
	 
	
	// Creating a LOCAL
	
	public long createLocal(Local local) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NOMBRE,local.getNombre());
		values.put(KEY_LATITUD,local.getLatitud());
		values.put(KEY_LONGITUD, local.getLongitud());
		values.put(KEY_PLANTA, local.getPlanta());
		
		long local_id = db.insert(TABLE_LOCAL, null, values);
		return local_id;
	}
	
	//Fetching all LOCALES
	
	public List<Local> getAllLocales() {
		List<Local> locales = new ArrayList<Local> ();
		String selectQuery = "SELECT * FROM " + TABLE_LOCAL;
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToFirst()) {
			do {
				Local loc = new Local();
				loc.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				loc.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
				loc.setLatitud(c.getString(c.getColumnIndex(KEY_LATITUD)));
				loc.setLongitud(c.getString(c.getColumnIndex(KEY_LONGITUD)));
				loc.setPlanta(c.getString(c.getColumnIndex(KEY_PLANTA)));
				locales.add(loc);
			} while (c.moveToNext());
		}
		return locales;
	}
	
	public Local getLocalFromOperador(Operador operador) {
		Local loc = new Local();
		//String selectQuery = "SELECT * FROM "+TABLE_LOCAL + " WHERE " + KEY_ID_OPERADOR + " = " + Integer.toString(operador.getBase_id()) + " ORDER BY " +KEY_PLANTA+ " LIMIT 1";
		String selectQuery = "SELECT * FROM "+TABLE_LOCAL + " WHERE " + KEY_ID_OPERADOR + " = " + Integer.toString(operador.getId()) + " ORDER BY " +KEY_PLANTA+ " LIMIT 1";
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	    
	    if(c.moveToFirst()) {
	    	loc.setId(c.getInt(c.getColumnIndex(KEY_ID)));
			loc.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
			loc.setLatitud(c.getString(c.getColumnIndex(KEY_LATITUD)));
			loc.setLongitud(c.getString(c.getColumnIndex(KEY_LONGITUD)));
			loc.setPlanta(c.getString(c.getColumnIndex(KEY_PLANTA)));
	    }
	    
	    return loc;
	}
	
	public List<Local> getLocalesPlanta(String planta) {
		List<Local> locales = new ArrayList<Local> ();
		String selectQ = "SELECT * FROM "+TABLE_LOCAL+ " WHERE " + KEY_PLANTA + " = "+planta;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQ, null);
		if(c.moveToFirst()) {
			do{
				Local loc = new Local();
				loc.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				loc.setNombre(c.getString(c.getColumnIndex(KEY_NOMBRE)));
				loc.setLatitud(c.getString(c.getColumnIndex(KEY_LATITUD)));
				loc.setLongitud(c.getString(c.getColumnIndex(KEY_LONGITUD)));
				loc.setPlanta(c.getString(c.getColumnIndex(KEY_PLANTA)));
				loc.setId_operador(c.getInt(c.getColumnIndex(KEY_ID_OPERADOR)));
				locales.add(loc);
			} while (c.moveToNext());
		}
		return locales;
	}
	
	 // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

}
