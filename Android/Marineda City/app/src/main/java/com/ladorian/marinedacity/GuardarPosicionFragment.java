package com.ladorian.marinedacity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class GuardarPosicionFragment extends Fragment implements OnCheckedChangeListener {
	private int nPlanta;
	
	
	
	private int getnPlanta() {
		return nPlanta;
	}

	private void setnPlanta(int nPlanta) {
		this.nPlanta = nPlanta;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.guardar_posicion, null);
		
		Button redButton = (Button)v.findViewById(R.id.red_button);
		Button greenButton = (Button)v.findViewById(R.id.green_button);
		Button malvaButton = (Button)v.findViewById(R.id.malva_button);
		Button blueButton = (Button)v.findViewById(R.id.blue_button);
		Button orangeButton = (Button)v.findViewById(R.id.orange_button);
		redButton.setOnClickListener(this.colorClick);
		greenButton.setOnClickListener(this.colorClick);
		malvaButton.setOnClickListener(this.colorClick);
		blueButton.setOnClickListener(this.colorClick);
		orangeButton.setOnClickListener(this.colorClick);
		nPlanta = R.id.button_one;
		
		final SegmentedRadioGroup rdioGroup = (SegmentedRadioGroup)v.findViewById(R.id.segmented_planta);
		rdioGroup.setOnCheckedChangeListener(this);
		
		return v;
	}
	
	private OnClickListener colorClick = new OnClickListener () {
		@Override
		public void onClick(View v) {
			parkingDialogFragment dialog = new parkingDialogFragment ();
			dialog.setBtnId(v.getId());
			dialog.setnPlanta(nPlanta);
			dialog.show(getFragmentManager(), "PlazaDialogFragment");
		}
	};

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		nPlanta = group.getCheckedRadioButtonId();	
	}
	
	

}
