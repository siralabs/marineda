package com.ladorian.marinedacity;

import com.ladorian.marinedacity.helper.EventListAdapter;
import com.ladorian.marinedacity.helper.OfertaListAdapter;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;


public class EventListFragment extends ListFragment {
	public String mTitulo;
	public String mMode;
	BaseAdapter adapter;

	public String getmTitulo() {
		return mTitulo;
	}



	public void setmTitulo(String mTitulo) {
		this.mTitulo = mTitulo;
	}



	public String getmMode() {
		return mMode;
	}



	public void setmMode(String mMode) {
		this.mMode = mMode;
	}
	
	
	

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		adapter = null;
	}



	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.shop_list, null);
		SearchView sv = (SearchView)v.findViewById(R.id.search_view);
		sv.setVisibility(View.GONE);
		ProgressBar pb = (ProgressBar)v.findViewById(R.id.progressBar1);
		TextView title = (TextView)v.findViewById(R.id.contentTitle);
		title.setText(mTitulo);
		ListView lv = (ListView)v.findViewById(android.R.id.list);
		if(mMode == "evento") {
			adapter = new EventListAdapter(getActivity(),pb);
		} else if(mMode == "oferta") {
			adapter = new OfertaListAdapter(getActivity(),pb);
		}
		lv.setAdapter(adapter);		    
		   
		return v;
	}



	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Object operadorInfo = l.getAdapter().getItem(position);
		((MainActivity)getActivity()).openEventDetail(operadorInfo);
	}
	
}
