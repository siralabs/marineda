package com.ladorian.marinedacity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.ladorian.marinedacity.helper.DatabaseHelper;
import com.ladorian.marinedacity.helper.DownloadImageTask;
import com.ladorian.marinedacity.model.Oferta;
import com.ladorian.marinedacity.model.Operador;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class DetailFragment extends ListFragment implements OnCheckedChangeListener {
	public String mTitulo;
	public Operador operador;
	private ListView dealList;
	private ShopDealsAdapter mAdapter;
	private ImageButton cityBtn;
	private SegmentedRadioGroup rdioGroup;
	
	public Operador getOperador() {
		return operador;
	}

	public void setOperador(Operador operador) {
		this.operador = operador;
	}

	public String getmTitulo() {
		return mTitulo;
	}

	public void setmTitulo(String mTitulo) {
		this.mTitulo = mTitulo;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
				
		View v = inflater.inflate(R.layout.detail_view, null);
		dealList = (ListView)v.findViewById(android.R.id.list);
		dealList.setVisibility(View.INVISIBLE);
		mAdapter = new ShopDealsAdapter(operador.getBase_id());
		dealList.setAdapter(mAdapter);
		ImageButton closeButton = (ImageButton)v.findViewById(R.id.closebtn);
		TextView title = (TextView)v.findViewById(R.id.contentTitle);
		title.setText(operador.getNombre());
		TextView nombre = (TextView)v.findViewById(R.id.nombre);
		nombre.setText(operador.getNombre());
		TextView local = (TextView)v.findViewById(R.id.local);
		local.setText("Local "+operador.getLocal());
		TextView telefono = (TextView)v.findViewById(R.id.telefono);
		telefono.setText(operador.getTelefono());
		telefono.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				String phoneNumber = (operador.getTelefono()).replaceAll("\\s+","");
				Log.d("phonecall",phoneNumber);
				callIntent.setData(Uri.parse("tel:"+phoneNumber));
				startActivity(callIntent);
			}
			
		});
		cityBtn = (ImageButton)v.findViewById(R.id.cityButton);
		cityBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MainActivity)getActivity()).openOfertasCity();
			}
			
		});
		cityBtn.setEnabled(false);
		cityBtn.setVisibility(View.INVISIBLE);
		TextView link = (TextView)v.findViewById(R.id.link);
		String cleanURL = (operador.getLink()).replaceAll("(http://|https://|ftp://)", "");
		link.setText(cleanURL);
		if(link.getText().length()>30){
			link.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
		}
		link.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String url;
				/*if(!operador.getLink().startsWith("www.")&& !operador.getLink().startsWith("http://")){
					  url = "www."+operador.getLink();
					}*/
					if(!operador.getLink().startsWith("http://")){
					  url = "http://"+operador.getLink();
					} else {
						url = operador.getLink();
					}
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				try {
					startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		ImageView image = (ImageView)v.findViewById(R.id.detailImage);
		new DownloadImageTask(image)
		.execute(operador.getImagen());
		
		TextView descripcion = (TextView)v.findViewById(R.id.descripcion);
		descripcion.setText(operador.getDescripcion());
		closeButton.setOnClickListener(new View.OnClickListener(){
    	@Override
    	public void onClick(View v) {
    		((MainActivity)getActivity()).onBackPressed();
    	}
    	});
		
		
		rdioGroup = (SegmentedRadioGroup)v.findViewById(R.id.segmentedcontrol);
		rdioGroup.clearCheck();
		rdioGroup.setOnCheckedChangeListener(this);
		
		CenteredRadioImageButton dealButton = (CenteredRadioImageButton)v.findViewById(R.id.button_ofertas);
		dealButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(dealList.getVisibility() == View.VISIBLE) {
					dealList.setVisibility(View.INVISIBLE);
					rdioGroup.clearCheck();
				} else {
					dealList.setVisibility(View.VISIBLE);
				}
				
			}
			
		});
		
		final CenteredRadioImageButton favButton = (CenteredRadioImageButton)v.findViewById(R.id.button_favorito);
		if(operador.isFavorito()>0) {
			favButton.setEnabled(false);
			favButton.setClickable(false);
		}
		favButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
				int fav = db.operadorFavorito(operador.getId());
				db.closeDB();
				StringBuilder msg = new StringBuilder();
				final CenteredRadioImageButton b = (CenteredRadioImageButton) v;
				if(fav > 0 ) {
					msg.append("Has añadido "+operador.getNombre()+" a tus favoritos.");
					b.setEnabled(false);
					b.setClickable(false);
		
				} else {
					msg.append("No se ha podido añadir " + operador.getNombre()+" a tus favoritos. Diculpa las molestias.");
				}
				b.setChecked(false);
				b.setSelected(false);
				new AlertDialog.Builder(getActivity())
			    .setTitle("Marineda City")
			    .setMessage(msg)
			    .setPositiveButton("Aceptar", null)
			    .show();
			}
			
		});
		
		CenteredRadioImageButton mapButton = (CenteredRadioImageButton)v.findViewById(R.id.button_plano);
	
		mapButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				CenteredRadioImageButton b = (CenteredRadioImageButton) v;
				b.setChecked(false);
				b.setSelected(false);
				Operador op = operador;
				((MainActivity)getActivity()).openShopMap(op);				
			}
			
		});
		return v;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch(checkedId) {
		case R.id.button_plano:
			dealList.setVisibility(View.INVISIBLE);
			group.clearCheck();
			break;
			
		case R.id.button_favorito:
			dealList.setVisibility(View.INVISIBLE);
			group.clearCheck();
			break;
			
		}
		
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		rdioGroup.clearCheck();
		Oferta ofertaInfo = getOfertaData(mAdapter.getItem(position));
		((MainActivity)getActivity()).openEventDetail(ofertaInfo);
	}
	
	
	private class ShopDealsAdapter extends BaseAdapter {
		
		private ArrayList<Map<String,String>> data;
		
		public ShopDealsAdapter(int base_id) {
			data = new ArrayList<Map<String,String>> ();
			ApiClient.getMarinedaCityApiClient().getShopDeals(Integer.toString(base_id), new Callback<Map<String,Object>> () {

				@Override
				public void failure(RetrofitError arg0) {
					
				}

				@Override
				public void success(Map<String,Object> arg0, Response arg1) {
					List<Map<String,String>> listado = (List<Map<String,String>>) arg0.get("ofertas");
					if(listado != null)
					addOfertas(listado);
					if(arg0.get("city").equals("YES")) {
						cityBtn.setVisibility(View.VISIBLE);
						cityBtn.setEnabled(true);
					}
				}
				
			});
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Map<String,String> getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(convertView == null) {
				convertView = inflater.inflate(R.layout.shop_ofertas_cell, null);
				TextView nombre = (TextView)convertView.findViewById(R.id.deal_name);
				nombre.setText(this.getItem(position).get("nombre"));
				
				SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
				Date fechaInicio = null;
				Date fechaFin = null;
				StringBuilder inicio = new StringBuilder();
				StringBuilder fin = new StringBuilder();
				
				inicio.append(this.getItem(position).get("inicio"));
				fin.append(this.getItem(position).get("fin"));
				
				try {
					fechaInicio = df.parse(inicio.toString());
					fechaFin = df.parse(fin.toString());
				}catch (ParseException ex){
					ex.printStackTrace();
				}
				SimpleDateFormat df2 = new SimpleDateFormat("dd/MM");
				String fechaInicio2 = df2.format(fechaInicio);
				String fechaFin2 = df2.format(fechaFin);
				TextView fecha = (TextView)convertView.findViewById(R.id.deal_date);
				fecha.setText("Del "+fechaInicio2+" al "+fechaFin2);				
			}
			return convertView;
		}
		
		public void addOfertas(List<Map<String,String>> ofertas) {
			this.data.clear();
			if(ofertas != null) {
				this.data.addAll(ofertas);
				if(getActivity()!=null) {
					getActivity().runOnUiThread(new Runnable () {
						public void run() {
							notifyDataSetChanged();
						}
					   });
				}
			}
			
		}
		
	}
	
	private Oferta getOfertaData(Map<String,String> data) {
		Oferta oferta = new Oferta();
		oferta.setNombre(data.get("nombre"));
		oferta.setDescripcion(data.get("descripcion"));
		oferta.setInicio(data.get("inicio"));
		oferta.setFin(data.get("fin"));
		oferta.setDetalle(data.get("detalle"));
		oferta.setImagen(data.get("imagen"));
		oferta.setOperador(data.get("operador"));
		oferta.setMiniatura(data.get("miniatura"));
		oferta.setIdoperador(data.get("idoperador"));
		return oferta;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mAdapter = null;
	}
	
	
	
	
	
	

}
