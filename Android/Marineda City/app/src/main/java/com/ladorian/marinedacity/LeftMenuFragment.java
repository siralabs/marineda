package com.ladorian.marinedacity;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnGroupClickListener;

public class LeftMenuFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.menu_sample, null);
        final ExpandableListView elv = (ExpandableListView) v.findViewById(R.id.list);
        elv.setAdapter(new LeftMenuListAdapter());
        elv.setOnGroupClickListener(new OnGroupClickListener(){

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				LeftMenuListAdapter adapter = (LeftMenuListAdapter) elv.getExpandableListAdapter();
				if(adapter.getChildrenCount(groupPosition) == 0) {
					Log.d("GROUPCOUNT","NO CHILDREN");
					((MainActivity)getActivity()).changeFragment(groupPosition,0,adapter.groups[groupPosition],0);
				}
				return false;
			}
        	
        });
        elv.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				LeftMenuListAdapter adapter = (LeftMenuListAdapter) elv.getExpandableListAdapter();
				((MainActivity)getActivity()).changeFragment(groupPosition, childPosition, adapter.children[groupPosition][childPosition],0);
				return false;
			}
        	
        });
       
        return v;
	}
	public class LeftMenuListAdapter extends BaseExpandableListAdapter {
        private final String[] groups = { "SHOPPING", "OCIO Y RESTAURACIÓN", "CARTELERA", /*"LO MÁS PRÓXIMO",*/"AGENDA","PROMOCIONES","MARINEDA CITY CARD","LOCALIZAR MI COCHE","LOCALIZACIÓN" };
 
        private final String[][] children = {
            { "VER TIENDAS (A-Z)", "MODA", "COMPLEMENTOS", "BELLEZA","HOGAR","T.ESPECIALIZADA","DEPORTES","SERVICIOS","HIPERMERCADO","GRANDES ALMACENES" },
            { },
            { },
            /*{ },*/
            { },
            { },
            { },
            {"GUARDAR POSICIÓN","RECORDAR POSICIÓN"},
            //{"ESTOY AQUÍ","IR A...","PLANO"}
            {"ESTOY AQUÍ","PLANO"}
        };
        

		@Override
        public int getGroupCount() {
            return groups.length;
        }
 
        @Override
        public int getChildrenCount(int i) {
            return children[i].length;
        }
 
        @Override
        public Object getGroup(int i) {
            return groups[i];
        }
 
        @Override
        public Object getChild(int i, int i1) {
            return children[i][i1];
        }
 
        @Override
        public long getGroupId(int i) {
            return i;
        }
 
        @Override
        public long getChildId(int i, int i1) {
            return i1;
        }
 
        @Override
        public boolean hasStableIds() {
            return true;
        }
 
        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        	if (convertView == null) {
        		   convertView = getActivity().getLayoutInflater().inflate(R.layout.group_row, null);
        		  }
        	TextView textView = (TextView)convertView.findViewById(R.id.row_name);
        	textView.setText(getGroup(groupPosition).toString());
        	ImageView iconView = (ImageView)convertView.findViewById(R.id.left_menu_icon);
        	switch(groupPosition){
        		case 0:
        			iconView.setImageResource(R.drawable.shopping);
        			break;
        		case 1:
        			iconView.setImageResource(R.drawable.restauracion);
        			break;
        		case 2:
        			iconView.setImageResource(R.drawable.ocio);
        			break;
        		/*case 3:
        			iconView.setImageResource(R.drawable.lomasproximo);
        			break;*/
        		case 3:
        			iconView.setImageResource(R.drawable.agenda);
        			break;
        		case 4:
        			iconView.setImageResource(R.drawable.promociones);
        			break;
        		case 5:
        			iconView.setImageResource(R.drawable.marinedacitycard);
        			break;
        		case 6:
        			iconView.setImageResource(R.drawable.localizarmicoche);
        			break;
        		case 7:
        			iconView.setImageResource(R.drawable.localizacion);
        			break;
        	}
        	/*TextView textView = new TextView(LeftMenuFragment.this.getActivity());
        	if(getChildrenCount(groupPosition) == 0) {
        		textView.setText(getGroup(groupPosition).toString() + "no elements");
        	} else {            
        		textView.setText(getGroup(groupPosition).toString());
        	}
            return textView;*/
        	return convertView;
        }
        
        /*public View getGroupView (int groupPosition, 
    			boolean isExpanded, 
    			View convertView, 
    			ViewGroup parent) {
        	TextView textView = new TextView(LeftMenuFragment.this.getActivity());
    		View v = this.getGroupView( groupPosition, isExpanded, convertView, parent);
    		View ind = v.findViewById( R.id.explist_indicator);
    		if( ind != null ) {
    			ImageView indicator = (ImageView)ind;
    			if( getChildrenCount( groupPosition ) == 0 ) {
    				indicator.setVisibility( View.INVISIBLE );
    			} else {
    				indicator.setVisibility( View.VISIBLE );
    				int stateSetIndex = ( isExpanded ? 1 : 0) ;
    				Drawable drawable = indicator.getDrawable();
    				drawable.setState(GROUP_STATE_SETS[stateSetIndex]);
    			}
    		}
    		return v;
    	}*/
 
        /*@Override
        public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
            TextView textView = new TextView(LeftMenuFragment.this.getActivity());
            textView.setText(getChild(i, i1).toString());
            return textView;
        }*/
        
 
        @Override
        public boolean isChildSelectable(int i, int i1) {
            return true;
        }

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			if (convertView == null) {
     		   convertView = getActivity().getLayoutInflater().inflate(R.layout.child_row, null);
     		  }
			TextView textView = (TextView)convertView.findViewById(R.id.child_row_name);
        	textView.setText(getChild(groupPosition,childPosition).toString());
        	ImageView iconView = (ImageView)convertView.findViewById(R.id.child_left_menu_icon);
        	if(groupPosition == 0) {
        		switch(childPosition) {
        			case 0:
        				iconView.setImageResource(R.drawable.vertiendas);
            			break;
        			case 1:
        				iconView.setImageResource(R.drawable.moda);
        				break;
        			case 2:
        				iconView.setImageResource(R.drawable.complementos);
        				break;
        			case 3:
        				iconView.setImageResource(R.drawable.belleza);
        				break;
        			case 4:
        				iconView.setImageResource(R.drawable.hogar);
        				break;
        			case 5:
        				iconView.setImageResource(R.drawable.tespecializada);
        				break;
        			case 6:
        				iconView.setImageResource(R.drawable.deportes);
        				break;
        			case 7:
        				iconView.setImageResource(R.drawable.servicios);
        				break;
        			case 8:
        				iconView.setImageResource(R.drawable.hipermercado);
        				break;
        			case 9:
        				iconView.setImageResource(R.drawable.grandesalmacenes);
        				break;
        		}
        	} else if (groupPosition == 6) {
        		switch(childPosition) {
	        		case 0:
	        			iconView.setImageResource(R.drawable.guardarposicion);
	        			break;
	        		case 1:
	        			iconView.setImageResource(R.drawable.recordarposicion);
	        			break;
        		}
        	} else if (groupPosition == 7) {
        		/*switch(childPosition) {
	        		case 0:
	        			iconView.setImageResource(R.drawable.estoyaqui);
	        			break;
	        		case 1:
	        			iconView.setImageResource(R.drawable.ira);
	        			break;
	        		case 2:
	        			iconView.setImageResource(R.drawable.plano);
	        			break;
        		}*/
        		switch(childPosition) {
        		case 0:
        			iconView.setImageResource(R.drawable.estoyaqui);
        			break;
        		case 1:
        			iconView.setImageResource(R.drawable.plano);
        			break;
        		
        		}
        	}
			return convertView;
		}
        
 
    }

}
