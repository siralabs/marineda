package com.ladorian.marinedacity;

import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

public class parkingDialogFragment extends DialogFragment {
	
	public interface NoticeDialogListener {
		public void onDialogPositiveClick(DialogFragment dialog);
		public void onDialogNegativeClick(DialogFragment dialog);
		//Add en vez de GCMPrivateKeys
		// APP_ID. GIVEN BY SEEKETING
		public final long APP_ID = 20142014133L;

		// SENDER_ID. API CONSOLE IDENTIFIER (OVERVIEW API CONSOLE)
		public final String SENDER_ID = "1065099229089";

		// SERVER_KEY. CREDENTIAL NEEDED IN ORDER TO USE GCM SERVICE (API CONSOLE)
		public final String SERVER_KEY = "AIzaSyBK-NE5fkB_WvPNO1WgDWakUp1oXsOJNak";

		// CONSTANTS NEEDED FOR NOTIFICATIONS CONTENT AND STRUCTURE

		public static final String KEY_ID = "notifId";
		public static final String MSG = "msg";
		public static final String ID = "id";
		public static final String TITLE = "We Observe!";
		public static final String SETS_INI_APP = "StartAppNoty";
	}
	
	NoticeDialogListener mListener;
	
	public int btnId;
	public int nPlanta;
	
	public String mPlanta;
	public String mColor;
	public String mPlaza;
	public String mLetra;
	
	public int getBtnId() {
		return btnId;
	}



	public void setBtnId(int btnId) {
		this.btnId = btnId;
	}

	

	public int getnPlanta() {
		return nPlanta;
	}



	public void setnPlanta(int nPlanta) {
		this.nPlanta = nPlanta;
	}



	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		// Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
	}

	private NumberPicker charPicker;
	private NumberPicker numPicker;
	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String[] alphabet = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.parking_dialog, null);
		charPicker = (NumberPicker)v.findViewById(R.id.charPicker);
		charPicker.setMinValue(0);
		charPicker.setValue(0);
		mColor = getResources().getResourceEntryName(btnId);
		if(nPlanta == R.id.button_one) {
			mPlanta = "-1";
			switch (btnId) {
				case R.id.red_button:
					charPicker.setMaxValue(10);
					charPicker.setDisplayedValues(alphabet);
					break;
				case R.id.green_button:
					charPicker.setMaxValue(10);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,22));
					break;
					
				case R.id.malva_button:					
					charPicker.setMaxValue(9);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,17,27));
					break;
				case R.id.blue_button:
					charPicker.setMaxValue(10);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,22));
					break;
				case R.id.orange_button:
					charPicker.setMaxValue(10);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,22));
					break;
			}
		} else if(nPlanta == R.id.button_two) {
			mPlanta = "-2";
			switch (btnId) {
				case R.id.red_button:
					charPicker.setMaxValue(11);
					charPicker.setDisplayedValues(alphabet);
					break;
				case R.id.green_button:
					charPicker.setMaxValue(7);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,19));
					break;
					
				case R.id.malva_button:					
					charPicker.setMaxValue(8);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,18,27));
					break;
				case R.id.blue_button:
					charPicker.setMaxValue(10);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,22));
					break;
				case R.id.orange_button:
					charPicker.setMaxValue(10);
					charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,22));
					break;
			}
		} else if(nPlanta == R.id.button_three) {
			mPlanta = "-3";
			switch (btnId) {
			case R.id.red_button:
				charPicker.setMaxValue(11);
				charPicker.setDisplayedValues(alphabet);
				break;
			case R.id.green_button:
				charPicker.setMaxValue(7);
				charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,19));
				break;
				
			case R.id.malva_button:					
				charPicker.setMaxValue(8);
				charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,18,27));
				break;
			case R.id.blue_button:
				charPicker.setMaxValue(10);
				charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,22));
				break;
			case R.id.orange_button:
				charPicker.setMaxValue(10);
				charPicker.setDisplayedValues(Arrays.copyOfRange(alphabet,11,22));
				break;
		}
	}
		
		
		numPicker = (NumberPicker)v.findViewById(R.id.numeroPicker);
		numPicker.setMinValue(1);
		numPicker.setMaxValue(41);
		builder.setMessage("Selecciona tu plaza")
		.setView(v)
        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            	String[] letras = charPicker.getDisplayedValues();
            	mLetra = letras[charPicker.getValue()];
            	
            	mPlaza = Integer.toString(numPicker.getValue());
                mListener.onDialogPositiveClick(parkingDialogFragment.this);
            }
        })
        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
		
		return builder.create();
	}

}
