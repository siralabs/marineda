package com.ladorian.marinedacity;

public class Constants {
	public static final String WEBSERVICEURL = "http://www.marinedaciti.org/ws";
	public static final String REGISTER_USER = "/user/register";
	public static final String UPDATE_USER = "/user/update";
	public static final String LOGIN_USER = "/user/login";
	public static final String GET_OPERADORES = "/operadores";
	public static final String UPDATE_OPERADORES = "/operadores/update";
	public static final String GET_OFERTAS = "/ofertas";
	public static final String GET_EVENTOS = "/eventos";
	public static final String GET_PUBLICIDAD = "/publicidad";
	public static final String GET_CITY_CARD = "/cityCard";
	public static final String GET_CITY_OFERTAS = "/cityCard/ofertas";
	public static final String GET_REVISTA = "/revista";
	public static final String[] INTERESES_STRING = {"Deportes","Ocio","Cocina","Cine","Hostelería"};
	public static final String COORDENADAS_URL = "http://www.marinedaciti.org/marinedaFiles/coordenadas/MapCoordinates.json";
	public static final String PUBLI_URL = "/publicidad/ios";
	public static final String REGISTER_NEWSLETTER = "/newsletter/suscribe";
}
