package com.ladorian.marinedacity;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class NewsletterFragment extends Fragment implements ValidationListener,OnEditorActionListener {
	private Validator validator;
	@Email(order=1,message="Email no válido")
	private EditText emailEdit;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		validator = new Validator(this);
		validator.setValidationListener(this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = (View)inflater.inflate(R.layout.newsletter, null);
		v.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hideKeyboard(v);
				return false;
			}
			
		});
		emailEdit = (EditText)v.findViewById(R.id.emailText);
		emailEdit.setOnEditorActionListener(this);
		Button submit = (Button)v.findViewById(R.id.submitBtn);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				validator.validate();
				
			}
			
		});
		return v;
	}

	@Override
	public void onValidationSucceeded() {
		// ENVIAR EMAIL DE REGISTRO E INCLUIR VARIABLE EN SHAREDPREFERENCES
		new NewsletterRegisterTask().execute(emailEdit.getText().toString());
		
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
		
	}
	
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
			v.clearFocus();
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
           return true;
        }
        return false;
	   }
	
	protected void hideKeyboard(View view) {
		 InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		 in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	private class NewsletterRegisterTask extends AsyncTask<String,Void,JSONObject> {

		@Override
		protected JSONObject doInBackground(String... params) {
			 String str = "";
		        HttpResponse response;
		        HttpClient myClient = new DefaultHttpClient();
		        HttpPost myConnection = new HttpPost(Constants.WEBSERVICEURL+Constants.REGISTER_NEWSLETTER);
		        JSONObject json = new JSONObject();		         
		        try {
		        	json.accumulate("email", params[0]);
		        	String object = json.toString();
		        	StringEntity se = new StringEntity(object);
		            myConnection.setEntity(se);
		            myConnection.setHeader("Accept","application/json");
		            myConnection.setHeader("Content-type","application/json");
		            response = myClient.execute(myConnection);
		            str = EntityUtils.toString(response.getEntity(), "UTF-8");
		            return new JSONObject(str);
		        } catch (ClientProtocolException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		        } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        return null;
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result!=null) {
				try {
					Boolean error = result.getBoolean("error");
					String message = result.getString("message");
					if(error) {
						Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		
	}

}
