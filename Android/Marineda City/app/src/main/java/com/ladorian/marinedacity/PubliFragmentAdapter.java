package com.ladorian.marinedacity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import com.ladorian.marinedacity.model.Evento;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

public class PubliFragmentAdapter extends FragmentPagerAdapter {
	private int[] Images = new int[] {R.drawable.publi1,R.drawable.publi2};
	public ArrayList<String> data = new ArrayList<String>();
    private int mCount = Images.length;
    private Context mContext;

    public PubliFragmentAdapter(FragmentManager fm,Context c) {
        super(fm);
        mContext = c;
        new GetPubliTask(this).execute();
    }
	@Override
	public Fragment getItem(int position) {
		String image;
		if(data!=null && data.size()>0){
				image = data.get(position);
		} else {
			image = "no image";
		}
		return new PublicidadFragment(position,this);
	}
	
	

	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return POSITION_NONE;
	}
	@Override
	public int getCount() {
		return data.size();
	}
	
	public void addPubli(ArrayList<String> publi) {
		this.data.clear();
		this.data.addAll(publi);
		((MainActivity) mContext).runOnUiThread(new Runnable () {
			public void run() {
				notifyDataSetChanged();
			}
		   });
		
	}
	
	public class GetPubliTask extends AsyncTask<Void,Void,JSONArray>{
		
		private PubliFragmentAdapter mAdapter;
		
		public GetPubliTask(PubliFragmentAdapter adapter) {
			mAdapter = adapter;
		}

		@Override
		protected JSONArray doInBackground(Void... params) {
			JSONArray json = null;
	        String str = "";
	        HttpResponse response;
	        HttpClient myClient = new DefaultHttpClient();
	        HttpGet myConnection = new HttpGet(Constants.WEBSERVICEURL+Constants.PUBLI_URL);
	        try {
	            response = myClient.execute(myConnection);
	            str = EntityUtils.toString(response.getEntity(), "UTF-8");
	            json = new JSONArray(str);
	             
	        } catch (ClientProtocolException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	                 
			return json;
		}

		@Override
		protected void onPostExecute(JSONArray result) {
			ArrayList<String> listado = new ArrayList<String>();
			for(int i=0;i<result.length();i++){
				try {
					listado.add(result.getJSONObject(i).getString("imagen"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			mAdapter.addPubli(listado);
		}		
		
	}

}
